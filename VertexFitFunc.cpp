#include "VertexFitFunc.h"
#include "NewtonMinimizerGradHessian.h"

#include <math.h>

using namespace std;
using namespace FitNewton;
using namespace Eigen;


HelixDCAFunc::HelixDCAFunc() : FunctionGradHessian(1, 8)
{
  tangent.assign(3,0.);
  point.assign(3,0.);
}


HelixDCAFunc::~HelixDCAFunc()
{
  
}


bool HelixDCAFunc::calcValGradHessian(const VectorXd& x, double& val, VectorXd& grad, MatrixXd& hessian)
{
//   the 5 helix parameters are k,d,phi,z0,dzdl
//   l will be used as the independent variable
//   l=0 corresponds to (d*cos(phi), d*sin(phi), z0)
//   fixedpars[5,6,7] correspond to vx,vy,vz of the point we are calculating the DCA to
//   
//   a change in l corresponds to what change in azimuth?
//   
//   l^2 = s^2 + z^2
//   
//   1 = s^2/l^2 + dzdl^2
//   
//   s^2 = (1 - dzdl^2)*l^2
//   
//   change in s gives what change in azimuth?
//   
//   s = theta*r = theta/k
//   
//   theta = s*k
//   
//   s = 2.*asin(0.5*k*D)/k;
//   
//   s*k*0.5 = asin(0.5*k*D)
//   
//   sin(s*k/2) = k*D/2
//   
//   D = (2/k)*sin(s*k/2)
//   
//   
//   in what direction do we move by D in the xy plane?
//   
//   theta/2 = asin(0.5*k*D)
//   
//   l == x
  
  
  
  double k = fixedpars[0];
  double d = fixedpars[1];
  double phi = fixedpars[2];
  double cosphi = cos(phi);
  double sinphi = sin(phi);
  //determine starting point on helix xp,yp,zp
  double xp = d*cosphi;
  double yp = d*sinphi;
  double zp = fixedpars[3];
  //determine tangent direction ux,uy in xy plane
  double ux = sinphi;
  double uy = -cosphi;
  if(d <= 0.)
  {
    ux=-ux;
    uy=-uy;
  }
  
  double dzdl = fixedpars[4];
  //determine point on the helix with parameter l == x(0)
  double dsdl = sqrt(1. - dzdl*dzdl);
  double s = dsdl*x(0);
  
  double psi = 0.5*s*k;
  double D = 0.;
  if(psi > 0.1)
  {
    D = (2./k)*sin(psi);
  }
  else
  {
    //sin(del) = del - del^3/6 + del^5/120 - del^7/5040. + ...
    //(2/k)*sin(s*k/2) = s - s^3*(k/2)^2/6 + s^5*(k/2)^4/120 - s^7*(k/2)^6/5040
    
    double srun=s;
    double s2 = s*s;
    double khalf2 = 0.5*k;khalf2*=khalf2;
    double krun = khalf2;
    
    D = s;
    srun *= s2;
    D -= srun*krun/6.;
    srun *= s2;
    krun *= khalf2;
    d += srun*krun/120.;
    srun *= s2;
    krun *= khalf2;
    D -= srun*krun/5040.;
  }
  if( ((int)((floor(fabs(s*k/M_PI)))))%2 == 1  )
  {
    psi = M_PI - psi;
  }
  double cospsi = cos(psi);
  double sinpsi = sin(psi);
  double ux2 = cospsi*ux - sinpsi*uy;
  double uy2 = sinpsi*ux + cospsi*uy;
  xp += D*ux2;
  yp += D*uy2;
  zp += fixedpars[4]*x(0);
  //calculate the new tangent angle
  if(psi>0. || psi < M_PI){psi = 2.*psi;}
  else if(psi < 0.)
  {
    psi = -psi;
    psi = M_PI - psi;
    psi = 2.*psi;
  }
  else
  {
    psi -= M_PI;
    psi = 2.*psi;
  }
  cospsi = cos(psi);
  sinpsi = sin(psi);
  ux2 = cospsi*ux - sinpsi*uy;
  uy2 = sinpsi*ux + cospsi*uy;
  
  
  
  
//   f = (vx - xp)^2 + (vy - yp)^2 + (vz - zp)^2
//   df/dl = 2*(vx - xp)*dxp/dl + 2*(vy - yp)*dyp/dl + 2*(vz - zp)*dzp/dl
//   
//   
//   dx/dt = r*ux2 = ux2/k
//   dy/dt = r*uy2 = uy2/k
//   
//   dx/ds = ux2
//   dy/ds = uy2
//   
//   dx/dl = (dx/ds)*(ds/dl)
//   
//   s^2 = (1 - dzdl^2)*l^2
//   
//   s = sqrt(1 - dzdl^2)*l
//   
//   ds/dl = sqrt(1 - dzdl^2)
//   
//   
//   x = cx + r*cos(t)
//   y = cy + r*sin(t)
//   
//   ux2 = -sin(t)
//   uy2 = cos(t)
//   
//   dx/dl = sqrt(1 - dzdl^2)*ux2
//   d^2x/dl^2 = sqrt(1 - dzdl^2)*d(ux2)/dl
//   
//   dux2/dt = -cos(t) = -uy2
//   dt/ds = k
//   ds/dl = sqrt(1 - dzdl^2)
//   
//   d^2x/dl^2 = -(1 - dzdl^2)*uy2*k
//   
//   d^2y/dl^2 = sqrt(1 - dzdl^2)*d(uy2)/dl
//   d^2y/dl^2 = (1 - dzdl^2)*ux2*k
//   
//   f = (vx - xp)^2 + (vy - yp)^2 + (vz - zp)^2
//   df/dl = -2*(vx - xp)*dxp/dl + -2*(vy - yp)*dyp/dl + -2*(vz - zp)*dzp/dl
//   d^2f/dl^2 = 2*(dxp/dl)^2 + -2*(vx-xp)*d^2xp/dl^2 + ditto for y and z
//   
//   dzp/dl = dzdl
  
//   cout<<"xp = "<<xp<<endl;
//   cout<<"yp = "<<yp<<endl;
//   cout<<"zp = "<<zp<<endl;
  
  double dx = xp - fixedpars[5];
  double dy = yp - fixedpars[6];
  double dz = zp - fixedpars[7];
  
//   cout<<"vx = "<<fixedpars[5]<<endl;
//   cout<<"vy = "<<fixedpars[6]<<endl;
//   cout<<"vz = "<<fixedpars[7]<<endl;
//   
//   cout<<"dx = "<<dx<<endl;
//   cout<<"dy = "<<dy<<endl;
//   cout<<"dz = "<<dz<<endl;
  
  val = dx*dx + dy*dy + dz*dz;
  
  grad(0) = 2.*dx*dsdl*ux2 + 2.*dy*dsdl*uy2 + 2.*dz*dzdl;
  
  hessian(0,0) = 2.*dsdl*ux2*dsdl*ux2;
  hessian(0,0) += 2.*dsdl*uy2*dsdl*uy2;
  hessian(0,0) += 2.*dzdl*dzdl;
  hessian(0,0) += -2.*dx*dsdl*dsdl*uy2*k;
  hessian(0,0) += 2.*dy*dsdl*dsdl*ux2*k;
  
//   cout<<"l = "<<x(0)<<endl;
//   cout<<"val = "<<val<<endl;
//   cout<<"grad = "<<grad(0)<<endl;
//   cout<<"hessian = "<<hessian(0,0)<<endl;
//   cout<<endl;
  
//   for(unsigned int par=0;par<8;par++)
//   {
//     cout<<fixedpars[par]<<" ";
//   }
//   cout<<x(0)<<" "<<val<<" "<<grad(0)<<" "<<hessian(0,0)<<endl;
  
  
  point[0] = xp;
  point[1] = yp;
  point[2] = zp;
  
  double xylen = sqrt(1-dzdl*dzdl);
  tangent[0] = ux2*xylen;
  tangent[1] = uy2*xylen;
  tangent[2] = dzdl;
  
  return true;
}





//fixedpars[0] is a gaussian regulator sigma
VertexFitFunc::VertexFitFunc() : FunctionGradHessian(3,1)
{
  
}


VertexFitFunc::~VertexFitFunc()
{
  
}


bool VertexFitFunc::calcValGradHessian(const VectorXd& x, double& val, VectorXd& grad, MatrixXd& hessian)
{
  NewtonMinimizerGradHessian minimizer;
  HelixDCAFunc helixfunc;
  minimizer.setFunction(&helixfunc);
  
  val = 0;
  for(int i=0;i<3;i++){grad(i)=0.;}
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      hessian(j,i)=0.;
    }
  }
  
  for(unsigned int i=0;i<tracks->size();i++)
  {
    helixfunc.setFixedPar(0, tracks->at(i).kappa);
    helixfunc.setFixedPar(1, tracks->at(i).d);
    helixfunc.setFixedPar(2, tracks->at(i).phi);
    helixfunc.setFixedPar(3, tracks->at(i).z0);
    helixfunc.setFixedPar(4, tracks->at(i).dzdl);
    helixfunc.setFixedPar(5, x(0));
    helixfunc.setFixedPar(6, x(1));
    helixfunc.setFixedPar(7, x(2));
    VectorXd start_point = VectorXd::Zero(1);
    VectorXd min_point = VectorXd::Zero(1);
    //find the point on the helix closest to the point x
    minimizer.minimize(start_point, min_point, 0x1.0p-30, 16, 0x1.0p-40);
    
    //now calculate the chi-square contribution from this track
    double tval=0.;
    VectorXd tgrad = VectorXd::Zero(1);
    MatrixXd thessian = MatrixXd::Zero(1,1);
    
    helixfunc.calcValGradHessian(min_point, tval, tgrad, thessian);
    
    vector<double> point, tangent;
    point.assign(3,0.);
    point[0] = helixfunc.getPoint(0);
    point[1] = helixfunc.getPoint(1);
    point[2] = helixfunc.getPoint(2);
    tangent.assign(3,0.);
    tangent[0] = helixfunc.getTangent(0);
    tangent[1] = helixfunc.getTangent(1);
    tangent[2] = helixfunc.getTangent(2);
    
    double t = tangent[0]*(x(0) - point[0]) + tangent[1]*(x(1) - point[1]) + tangent[2]*(x(2) - point[2]);
    
    vector<double> l;
    l.assign(3,0.);
    l[0] = point[0] + t*tangent[0];
    l[1] = point[1] + t*tangent[1];
    l[2] = point[2] + t*tangent[2];
    
    vector<double> d;
    d.assign(3,0.);
    d[0] = x(0) - l[0];
    d[1] = x(1) - l[1];
    d[2] = x(2) - l[2];
    
    double f = d[0]*d[0] + d[1]*d[1] + d[2]*d[2];;
    double g0 = -2.*tangent[0]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[0];
    double g1 = -2.*tangent[1]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[1];
    double g2 = -2.*tangent[2]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[2];
    double h0 = 2.*(1. - tangent[0]*tangent[0]);
    double h1 = 2.*(1. - tangent[1]*tangent[1]);
    double h2 = 2.*(1. - tangent[2]*tangent[2]);
    
    double invs2 = 1./(2.*fixedpars[0]*fixedpars[0]);
    
    double g = -exp(-f*invs2);
    
    val += g;
    
    double grd0 = -invs2*g*g0;
    double grd1 = -invs2*g*g1;
    double grd2 = -invs2*g*g2;
    
    grad(0) += grd0;
    grad(1) += grd1;
    grad(2) += grd2;
    
    hessian(0,0) += -invs2*( grd0*g0 + g*h0 );
    hessian(1,1) += -invs2*( grd1*g1 + g*h1 );
    hessian(2,2) += -invs2*( grd2*g2 + g*h2 );
    double htemp = -invs2*( grd1*g0);
    hessian(0,1) += htemp;
    hessian(1,0) += htemp;
    htemp = -invs2*( grd2*g0 );
    hessian(0,2) += htemp;
    hessian(2,0) += htemp;
    htemp = -invs2*( grd2*g1 );
    hessian(1,2) += htemp;
    hessian(2,1) += htemp;
    
    
//     val += d[0]*d[0] + d[1]*d[1] + d[2]*d[2];
//     
//     grad(0) += -2.*tangent[0]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[0];
//     grad(1) += -2.*tangent[1]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[1];
//     grad(2) += -2.*tangent[2]*( d[0]*tangent[0] + d[1]*tangent[1] + d[2]*tangent[2] ) + 2*d[2];
//     
//     hessian(0,0) += 2.*(1. - tangent[0]*tangent[0]);
//     hessian(1,1) += 2.*(1. - tangent[1]*tangent[1]);
//     hessian(2,2) += 2.*(1. - tangent[2]*tangent[2]);
  }
  
  return true;
}


