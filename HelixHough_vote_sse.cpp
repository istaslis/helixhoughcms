#include "HelixHough.h"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <sys/time.h>

using namespace std;

static const unsigned int digits = 2;
static const unsigned int r = 7;
static const unsigned int radix = 1 << r;
static const unsigned int mask = radix - 1;

static void radix_sort(std::vector<BinEntryPair5D>& A){
  unsigned int SIZE = A.size();
  std::vector<BinEntryPair5D> B(SIZE);
  std::vector<unsigned int> cnt(radix);
  
  for(unsigned int i = 0, shift = 0; i < digits; i++, shift += r){
    for(unsigned int j = 0; j < radix; ++j){
      cnt[j] = 0;
    }
    
    for(unsigned int j = 0; j < SIZE; ++j){
      ++cnt[(A[j].bin >> shift) & mask];
    }
    
    for(unsigned int j = 1; j < radix; ++j){
      cnt[j] += cnt[j - 1];
    }
    
    for(long j = SIZE - 1; j >= 0; --j){
      B[--cnt[(A[j].bin >> shift) & mask]] = A[j];
    }
    
    for(unsigned int j = 0; j < SIZE; ++j){
      A[j] = B[j]; 
    }
  }
}


static void radix_sort(unsigned int* A, unsigned int* B, unsigned int SIZE){
  unsigned int cnt[1<<7];
  
  for(unsigned int i = 0, shift = 0; i < digits; i++, shift += r){
    for(unsigned int j = 0; j < radix; ++j){
      cnt[j] = 0;
    }
    
    for(unsigned int j = 0; j < SIZE; ++j){
      ++cnt[((A[j]>>20) >> shift) & mask];
    }
    
    for(unsigned int j = 1; j < radix; ++j){
      cnt[j] += cnt[j - 1];
    }
    
    for(long j = SIZE - 1; j >= 0; --j){
      B[--cnt[((A[j]>>20) >> shift) & mask]] = A[j];
    }
    
    for(unsigned int j = 0; j < SIZE; ++j){
      A[j] = B[j]; 
    }
  }
}


static void counting_sort(unsigned int MAX, std::vector<BinEntryPair5D>& A){
  unsigned int SIZE = A.size();
  std::vector<BinEntryPair5D> B(SIZE);
  std::vector<unsigned int> C(MAX+1);
  
  for (unsigned int i = 0; i < SIZE; ++i){
    C[A[i].bin]+=1;
  }
  
  for (unsigned int i = 1; i <= MAX; ++i){
    C[i] += C[i - 1];
  }
  
  for (long i = SIZE - 1; i >= 0; --i) {
    B[C[A[i].bin] - 1] = A[i];
    --C[A[i].bin];
  }
  
  for (unsigned int i = 0; i < SIZE; ++i){
    A[i] = B[i];
  }
}


static void counting_sort(unsigned int SIZE, unsigned int MAX, unsigned int* A, unsigned int* B, unsigned int* C)
{
  for(unsigned int i=0;i<SIZE;++i)
  {
    C[(A[i])>>20]+=1;
  }
  for(unsigned int i=1;i<MAX;++i)
  {
    C[i] += C[i-1];
  }
  for(int i=(SIZE-1);i>=0;--i)
  {
    B[C[A[i]>>20]-1] = A[i];
    --C[A[i]>>20];
  }
  for(unsigned int i=0;i<SIZE;++i)
  {
    A[i] = B[i];
  }
}



void HelixHough::fillBins(unsigned int total_bins, unsigned int hit_counter, float*
min_phi_a, float* max_phi_a, vector<SimpleHit3D>& four_hits,
fastvec2d& z_bins, unsigned int n_d, unsigned int
n_k, unsigned int n_dzdl, unsigned int n_z0, unsigned int d_bin, unsigned int
k_bin, unsigned int n_phi, unsigned int zoomlevel, float low_phi, float
high_phi, float inv_phi_range, fastvec& vote_array)
{
  unsigned int buffer[(1<<10)];
  unsigned int bufnum = 0;
  
  unsigned int zbuffer[1<<10];
  unsigned int zbufnum[8];
  z_bins.fetch(four_hits[0].index, four_hits[(hit_counter-1)].index, zbuffer, zbufnum);
  unsigned int size2 = n_z0*n_dzdl;
  
  for(unsigned int i=0;i<hit_counter;++i)
  {
    unsigned int index = four_hits[i].index;
    if(min_phi_a[i] >= 0.)
    {
      if( ( max_phi_a[i] < low_phi ) || ( min_phi_a[i] > high_phi ) ){continue;}
      unsigned int low_bin = 0;
      unsigned int high_bin = (n_phi-1);
      if( min_phi_a[i] > low_phi )
      {
        low_bin = (unsigned int)(((min_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));
      }
      if( max_phi_a[i] < high_phi )
      {
        high_bin = (unsigned int)(((max_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));
      }
      for(unsigned int b=low_bin;b<=high_bin;++b)
      {
        unsigned int pos = i*size2;
        for(unsigned int zbin=0;zbin<zbufnum[i];++zbin)
        {
          unsigned int zint = zbuffer[pos];pos+=1;
          unsigned int bin = BinEntryPair5D::linearBin(n_d, n_k, n_dzdl, n_z0, b, d_bin, k_bin, zint>>16, zint&65535);
          buffer[bufnum] = ((bin<<20) ^ index);bufnum+=1;
        }
      }
    }
    else
    {
      if( ( high_phi < ( min_phi_a[i] + 2.*M_PI) ) && ( low_phi > max_phi_a[i] ) ){continue;}
      if( ( high_phi < ( min_phi_a[i] + 2.*M_PI) ) && ( low_phi <= max_phi_a[i] ) )
      {
        unsigned int low_bin = 0;
        unsigned int high_bin = (unsigned int)(((max_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));
        if(high_bin>(n_phi-1)){high_bin=(n_phi-1);}
        unsigned int index = four_hits[i].index;
        for(unsigned int b=low_bin;b<=high_bin;++b)
        {
          unsigned int pos = i*size2;
          for(unsigned int zbin=0;zbin<zbufnum[i];++zbin)
          {
            unsigned int zint = zbuffer[pos];pos+=1;
            unsigned int bin = BinEntryPair5D::linearBin(n_d, n_k, n_dzdl, n_z0, b, d_bin, k_bin, zint>>16, zint&65535);
            buffer[bufnum] = ((bin<<20) ^ index);bufnum+=1;
          }
        }
      }
      else if( ( high_phi >= ( min_phi_a[i] + 2.*M_PI) ) && ( low_phi > max_phi_a[i] ) )
      {
        unsigned int high_bin = (n_phi-1);
        unsigned int low_bin=0;
        if((2.*M_PI+min_phi_a[i]-low_phi)>0.){low_bin = (unsigned int)(((2.*M_PI+min_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));}
        unsigned int index = four_hits[i].index;
        for(unsigned int b=low_bin;b<=high_bin;++b)
        {
          unsigned int pos = i*size2;
          for(unsigned int zbin=0;zbin<zbufnum[i];++zbin)
          {
            unsigned int zint = zbuffer[pos];pos+=1;
            unsigned int bin = BinEntryPair5D::linearBin(n_d, n_k, n_dzdl, n_z0, b, d_bin, k_bin, zint>>16, zint&65535);
            buffer[bufnum] = ((bin<<20) ^ index);bufnum+=1;
          }
        }
      }
      else
      {
        //split into two sets of bins
        
        unsigned int high_bin = (n_phi-1);
        unsigned int low_bin=0;
        if((2.*M_PI+min_phi_a[i]-low_phi)>0.){low_bin = (unsigned int)(((2.*M_PI+min_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));}
        unsigned int index = four_hits[i].index;
        for(unsigned int b=low_bin;b<=high_bin;++b)
        {
          unsigned int pos = i*size2;
          for(unsigned int zbin=0;zbin<zbufnum[i];++zbin)
          {
            unsigned int zint = zbuffer[pos];pos+=1;
            unsigned int bin = BinEntryPair5D::linearBin(n_d, n_k, n_dzdl, n_z0, b, d_bin, k_bin, zint>>16, zint&65535);
            buffer[bufnum] = ((bin<<20) ^ index);bufnum+=1;
          }
        }
        
        low_bin = 0;
        high_bin = (unsigned int)(((max_phi_a[i]-low_phi)*inv_phi_range)*((float)(n_phi)));
        if(high_bin>(n_phi-1)){high_bin=(n_phi-1);}
        index = four_hits[i].index;
        for(unsigned int b=low_bin;b<=high_bin;++b)
        {
          unsigned int pos = i*size2;
          for(unsigned int zbin=0;zbin<zbufnum[i];++zbin)
          {
            unsigned int zint = zbuffer[pos];pos+=1;
            unsigned int bin = BinEntryPair5D::linearBin(n_d, n_k, n_dzdl, n_z0, b, d_bin, k_bin, zint>>16, zint&65535);
            buffer[bufnum] = ((bin<<20) ^ index);bufnum+=1;
          }
        }
      }
    }
  }
  vote_array.push_back(buffer, bufnum);
}


void HelixHough::vote_z(unsigned int zoomlevel, unsigned int n_phi, unsigned int n_d, unsigned int
n_k, unsigned int n_dzdl, unsigned int n_z0, fastvec2d& z_bins)
{
  float z0_size = (zoomranges[zoomlevel].max_z0 - zoomranges[zoomlevel].min_z0)/((float)n_z0);
  float dzdl_size = (zoomranges[zoomlevel].max_dzdl - zoomranges[zoomlevel].min_dzdl)/((float)n_dzdl);
  float low_phi = zoomranges[zoomlevel].min_phi;
  float high_phi = zoomranges[zoomlevel].max_phi;
  float low_dzdl = zoomranges[zoomlevel].min_dzdl;
  float high_dzdl = zoomranges[zoomlevel].max_dzdl;
  float dzdl_bin_size_inv = 1./dzdl_size;
  
  //cache cosine and sine calculations
  float min_cos = cos( zoomranges[zoomlevel].min_phi );
  float max_cos = cos( zoomranges[zoomlevel].max_phi );
  float min_sin = sin( zoomranges[zoomlevel].min_phi );
  float max_sin = sin( zoomranges[zoomlevel].max_phi );
  if( (high_phi - low_phi) > M_PI )
  {
    min_cos = 1.;
    max_cos = -1.;
    min_sin = 0.;
    max_sin = 0.;
  }
  
  unsigned int one_z_bin;
  float pwr = 0.5;
  float min_kappa = pow(zoomranges[zoomlevel].min_k, pwr);
  float max_kappa = pow(zoomranges[zoomlevel].max_k, pwr);
  
  unsigned int hit_counter = 0;
  float x_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float min_dzdl_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float max_dzdl_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz_a[4] = {0., 0., 0., 0.};
  vector<SimpleHit3D> four_hits;
  SimpleHit3D temphit(0.,0., 0.,0., 0.,0., 0, 0);
  four_hits.assign(4, temphit);
  unsigned int temp_zcount[4];
  unsigned buffer[4][1<<8];
  for(unsigned int i=0;i<hits_vec[zoomlevel]->size();i++)
  {
    x_a[hit_counter] = (*(hits_vec[zoomlevel]))[i].x;
    y_a[hit_counter] = (*(hits_vec[zoomlevel]))[i].y;
    z_a[hit_counter] = (*(hits_vec[zoomlevel]))[i].z;
    dz_a[hit_counter] = (*(hits_vec[zoomlevel]))[i].dz;
    four_hits[hit_counter] = (*(hits_vec[zoomlevel]))[i];
    
    hit_counter++;
    
    if(hit_counter==4)
    {
      for(unsigned int h=0;h<hit_counter;++h)
      {
        temp_zcount[h] = 0.;
      }
      
      for(unsigned int zz=0;zz<n_z0;++zz)
      {
        float min_z0 = zoomranges[zoomlevel].min_z0 + ((float)(zz))*z0_size;
        float max_z0 = min_z0 + z0_size;
        
        float dz = dz_a[0];
        for(unsigned int h=1;h<hit_counter;++h)
        {
          if(dz_a[h] > dz){dz = dz_a[h];}
        }
        min_z0 -= dz;
        max_z0 += dz;
        
        dzdlRange_sse(x_a, y_a, z_a, min_cos, min_sin, max_cos, max_sin, min_kappa, max_kappa, zoomranges[zoomlevel].min_d, zoomranges[zoomlevel].max_d, min_z0, max_z0, min_dzdl_a, max_dzdl_a);
        
        unsigned int low_bin = 0;
        unsigned int high_bin = 0;
        
        for(unsigned int h=0;h<hit_counter;++h)
        {
          float d_dzdl = dzdlError(four_hits[h], min_kappa, max_kappa, zoomranges[zoomlevel].min_d, zoomranges[zoomlevel].max_d, min_z0, max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
          
          float min_dzdl = min_dzdl_a[h] - d_dzdl;
          float max_dzdl = max_dzdl_a[h] + d_dzdl;
          
          if((min_dzdl > high_dzdl) || (max_dzdl < low_dzdl))
          {
            low_bin=1;
            high_bin=0;
          }
          else
          {
            if(min_dzdl > low_dzdl)
            {
              low_bin = (unsigned int)((min_dzdl - low_dzdl)*dzdl_bin_size_inv);
            }
            else
            {
              low_bin = 0;
            }
            
            if(max_dzdl < high_dzdl)
            {
              high_bin = (unsigned int)((max_dzdl - low_dzdl)*dzdl_bin_size_inv);
            }
            else
            {
              high_bin = n_dzdl - 1;
            }
          }
          for(unsigned int bb=low_bin;bb<=high_bin;bb++)
          {
            if(bb >= n_dzdl){continue;}
            one_z_bin = (zz ^ (bb<<16));
            buffer[h][temp_zcount[h]] = one_z_bin;
            temp_zcount[h] += 1.;
          }
        }
      }
      for(unsigned int h=0;h<hit_counter;++h)
      {
        z_bins.fill(buffer[h], temp_zcount[h]);
      }
      hit_counter=0;
    }
  }
  if(hit_counter != 0)
  {
    for(unsigned int h=0;h<hit_counter;++h)
    {
      temp_zcount[h] = 0.;
    }
    
    for(unsigned int zz=0;zz<n_z0;++zz)
    {
      float min_z0 = zoomranges[zoomlevel].min_z0 + ((float)(zz))*z0_size;
      float max_z0 = min_z0 + z0_size;
      
      float dz = dz_a[0];
      for(unsigned int h=1;h<hit_counter;++h)
      {
        if(dz_a[h] > dz){dz = dz_a[h];}
      }
      min_z0 -= dz;
      max_z0 += dz;
      
      dzdlRange_sse(x_a, y_a, z_a, min_cos, min_sin, max_cos, max_sin, min_kappa, max_kappa, zoomranges[zoomlevel].min_d, zoomranges[zoomlevel].max_d, min_z0, max_z0, min_dzdl_a, max_dzdl_a);
      
      unsigned int low_bin = 0;
      unsigned int high_bin = 0;
      
      for(unsigned int h=0;h<hit_counter;++h)
      {
        
        float d_dzdl = dzdlError(four_hits[h], min_kappa, max_kappa, zoomranges[zoomlevel].min_d, zoomranges[zoomlevel].max_d, min_z0, max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
        
        float min_dzdl = min_dzdl_a[h] - d_dzdl;
        float max_dzdl = max_dzdl_a[h] + d_dzdl;
        
        if((min_dzdl > high_dzdl) || (max_dzdl < low_dzdl))
        {
          low_bin=1;
          high_bin=0;
        }
        else
        {
          if(min_dzdl > low_dzdl)
          {
            low_bin = (unsigned int)((min_dzdl - low_dzdl)*dzdl_bin_size_inv);
          }
          else
          {
            low_bin = 0;
          }
          
          if(max_dzdl < high_dzdl)
          {
            high_bin = (unsigned int)((max_dzdl - low_dzdl)*dzdl_bin_size_inv);
          }
          else
          {
            high_bin = n_dzdl - 1;
          }
        }
        for(unsigned int bb=low_bin;bb<=high_bin;bb++)
        {
          if(bb >= n_dzdl){continue;}
          one_z_bin = (zz ^ (bb<<16));
          buffer[h][temp_zcount[h]] = one_z_bin;
          temp_zcount[h] += 1.;
        }
      }
    }
    for(unsigned int h=0;h<hit_counter;++h)
    {
      z_bins.fill(buffer[h], temp_zcount[h]);
    }
    hit_counter=0;
  }
}


void HelixHough::vote(unsigned int zoomlevel)
{
  fastvec vote_array;
  
  unsigned int n_phi = (*(nhits_array_vec[zoomlevel])).size();
  unsigned int n_d = (*(nhits_array_vec[zoomlevel]))[0].size();
  unsigned int n_k = (*(nhits_array_vec[zoomlevel]))[0][0].size();
  unsigned int n_dzdl = (*(nhits_array_vec[zoomlevel]))[0][0][0].size();
  unsigned int n_z0 = (*(nhits_array_vec[zoomlevel]))[0][0][0][0].size();
  
  fastvec2d z_bins(n_dzdl*n_z0);
  
  unsigned int total_bins = n_phi*n_d*n_k*n_dzdl*n_z0;
  
  float d_size = (zoomranges[zoomlevel].max_d - zoomranges[zoomlevel].min_d)/((float)n_d);
  float k_size = (zoomranges[zoomlevel].max_k - zoomranges[zoomlevel].min_k)/((float)n_k);
  float low_phi = zoomranges[zoomlevel].min_phi;
  float high_phi = zoomranges[zoomlevel].max_phi;
  float inv_phi_range = 1./(high_phi - low_phi);
  
  float pwr = 0.5;
  float min_kappa = pow(zoomranges[zoomlevel].min_k, pwr);
  float max_kappa = pow(zoomranges[zoomlevel].max_k, pwr);
  
  timeval t1,t2;
  double time1=0.;
  double time2=0.;
  gettimeofday(&t1, NULL);
  vote_z(zoomlevel, n_phi,n_d,n_k,n_dzdl,n_z0, z_bins);
  gettimeofday(&t2, NULL);
  time1 = ((double)(t1.tv_sec) + (double)(t1.tv_usec)/1000000.);
  time2 = ((double)(t2.tv_sec) + (double)(t2.tv_usec)/1000000.);
  z_vote_time += (time2 - time1);
  
  //now vote in xy
  __m128 phi_3_in;
  __m128 phi_4_in;
  __m128 phi_3_out;
  __m128 phi_4_out;
  __m128 phi_3_in_2;
  __m128 phi_4_in_2;
  __m128 phi_3_out_2;
  __m128 phi_4_out_2;
  unsigned int hit_counter = 0;
  float x_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float x_a_2[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y_a_2[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  vector<SimpleHit3D> four_hits;
  SimpleHit3D temphit(0.,0., 0.,0., 0.,0., 0, 0);
  four_hits.assign(4, temphit);
  vector<SimpleHit3D> four_hits_2;
  four_hits_2.assign(4, temphit);
  vector<SimpleHit3D> eight_hits;
  eight_hits.assign(8, temphit);
  float min_phi_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float max_phi_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float min_phi_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float max_phi_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float min_phi_8[8];
  float max_phi_8[8];
  gettimeofday(&t1, NULL);
  float min_k_array[1<<8];
  float max_k_array[1<<8];
  min_k_array[0] = zoomranges[zoomlevel].min_k;
  max_k_array[0] = zoomranges[zoomlevel].min_k + k_size;
  for(unsigned int k_bin=1;k_bin<n_k;++k_bin)
  {
    min_k_array[k_bin] = min_k_array[k_bin-1] + k_size;
    max_k_array[k_bin] = max_k_array[k_bin-1] + k_size;
  }
  for(unsigned int k_bin=0;k_bin<n_k;++k_bin)
  {
    min_k_array[k_bin] = pow(min_k_array[k_bin],pwr);
    max_k_array[k_bin] = pow(max_k_array[k_bin],pwr);
  }
  float min_d_array[1<<8];
  float max_d_array[1<<8];
  min_d_array[0] = zoomranges[zoomlevel].min_d;
  max_d_array[0] = zoomranges[zoomlevel].min_d + d_size;
  for(unsigned int d_bin=1;d_bin<n_d;++d_bin)
  {
    min_d_array[d_bin] = min_d_array[d_bin-1] + d_size;
    max_d_array[d_bin] = max_d_array[d_bin-1] + d_size;
  }
  for(unsigned int i=0;i<hits_vec[zoomlevel]->size();i++)
  {
    if(hit_counter < 4)
    {
      four_hits[hit_counter] = ((*(hits_vec[zoomlevel]))[i]);
      x_a[hit_counter] = four_hits[hit_counter].x;
      y_a[hit_counter] = four_hits[hit_counter].y;
      four_hits[hit_counter].index = i;
      eight_hits[hit_counter] = four_hits[hit_counter];
    }
    else
    {
      four_hits_2[hit_counter-4] = ((*(hits_vec[zoomlevel]))[i]);
      x_a_2[hit_counter-4] = four_hits_2[hit_counter-4].x;
      y_a_2[hit_counter-4] = four_hits_2[hit_counter-4].y;
      four_hits_2[hit_counter-4].index = i;
      eight_hits[hit_counter] = four_hits_2[hit_counter-4];
    }
    hit_counter++;
    if((hit_counter == 8) && (separate_by_helicity==true))
    {
      for(unsigned int d_bin=0;d_bin<n_d;++d_bin)
      {
        float min_d_a[4] __attribute__((aligned(16))) = {min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin]};
        float max_d_a[4] __attribute__((aligned(16))) = {max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin]};
        
        for(unsigned int k_bin=0;k_bin<n_k;++k_bin)
        {
          float min_k_a[4] __attribute__((aligned(16))) = {min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin]};
          float max_k_a[4] __attribute__((aligned(16))) = {max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin]};
          float hel=-1.;
          if(helicity==true){hel=1.;}
          if(k_bin==0)
          {
            HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, min_phi_2_a, max_phi_2_a, hel, phi_3_out, phi_4_out, x_a_2, y_a_2, phi_3_out_2, phi_4_out_2);
            
            phi_3_in = phi_3_out;
            phi_4_in = phi_4_out;
            phi_3_in_2 = phi_3_out_2;
            phi_4_in_2 = phi_4_out_2;
          }
          else
          {
            HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, min_phi_2_a, max_phi_2_a, hel, phi_3_in, phi_4_in, phi_3_out, phi_4_out, x_a_2, y_a_2, phi_3_in_2, phi_4_in_2, phi_3_out_2, phi_4_out_2);
            
            phi_3_in = phi_3_out;
            phi_4_in = phi_4_out;
            phi_3_in_2 = phi_3_out_2;
            phi_4_in_2 = phi_4_out_2;
          }
          for(unsigned int h=0;h<hit_counter;++h)
          {
            if(h<4)
            {
              float dphi = sqrt((four_hits[h].dx*four_hits[h].dx + four_hits[h].dy*four_hits[h].dy)/(four_hits[h].x*four_hits[h].x + four_hits[h].y*four_hits[h].y));
              dphi += phiError(four_hits[h], min_kappa, max_kappa, min_d_array[d_bin], max_d_array[d_bin], zoomranges[zoomlevel].min_z0, zoomranges[zoomlevel].max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
              
              min_phi_1_a[h] -= dphi;
              max_phi_1_a[h] += dphi;
              min_phi_8[h] = min_phi_1_a[h];
              max_phi_8[h] = max_phi_1_a[h];
            }
            else
            {
              float dphi = sqrt((four_hits_2[h-4].dx*four_hits_2[h-4].dx + four_hits_2[h-4].dy*four_hits_2[h-4].dy)/(four_hits_2[h-4].x*four_hits_2[h-4].x + four_hits_2[h-4].y*four_hits_2[h-4].y));
              dphi += phiError(four_hits[h-4], min_kappa, max_kappa, min_d_array[d_bin], max_d_array[d_bin], zoomranges[zoomlevel].min_z0, zoomranges[zoomlevel].max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
              
              min_phi_2_a[h-4] -= dphi;
              max_phi_2_a[h-4] += dphi;
              min_phi_8[h] = min_phi_2_a[h-4];
              max_phi_8[h] = max_phi_2_a[h-4];
            }
          }
          fillBins(total_bins, 8, min_phi_8, max_phi_8, eight_hits, z_bins,  n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
        }
      }
      hit_counter = 0;
    }
    if((hit_counter == 4) && ( ((hits_vec[zoomlevel]->size() - (i + 1))<4) || (separate_by_helicity==false) ))
    {
      for(unsigned int d_bin=0;d_bin<n_d;++d_bin)
      {
        float min_d_a[4] __attribute__((aligned(16))) = {min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin]};
        float max_d_a[4] __attribute__((aligned(16))) = {max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin]};
        
        for(unsigned int k_bin=0;k_bin<n_k;++k_bin)
        {
          float min_k_a[4] __attribute__((aligned(16))) = {min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin]};
          float max_k_a[4] __attribute__((aligned(16))) = {max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin]};
          if(separate_by_helicity==true)
          {
            float hel=-1.;
            if(helicity==true){hel=1.;}
            if(k_bin==0)
            {
              HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, hel, phi_3_out, phi_4_out);
              phi_3_in = phi_3_out;
              phi_4_in = phi_4_out;
            }
            else
            {
              HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, max_k_a, min_phi_1_a, max_phi_1_a, hel, phi_3_in, phi_4_in, phi_3_out, phi_4_out);
              phi_3_in = phi_3_out;
              phi_4_in = phi_4_out;
            }
          }
          else
          {
            HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, min_phi_2_a, max_phi_2_a);
          }
          for(unsigned int h=0;h<hit_counter;++h)
          {
            float dphi = sqrt((four_hits[h].dx*four_hits[h].dx + four_hits[h].dy*four_hits[h].dy)/(four_hits[h].x*four_hits[h].x + four_hits[h].y*four_hits[h].y));
            dphi += phiError(four_hits[h], min_kappa, max_kappa, min_d_array[d_bin], max_d_array[d_bin], zoomranges[zoomlevel].min_z0, zoomranges[zoomlevel].max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
            
            min_phi_1_a[h] -= dphi;
            min_phi_2_a[h] -= dphi;
            max_phi_1_a[h] += dphi;
            max_phi_2_a[h] += dphi;
          }
          if(separate_by_helicity==true)
          {
            fillBins(total_bins, hit_counter, min_phi_1_a, max_phi_1_a, four_hits, z_bins,  n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
          }
          else
          {
            fillBins(total_bins, hit_counter, min_phi_1_a, max_phi_1_a, four_hits, z_bins,  n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
            fillBins(total_bins, hit_counter, min_phi_2_a, max_phi_2_a, four_hits, z_bins, n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
          }
        }
      }
      hit_counter = 0;
    }
  }
  if(hit_counter != 0)
  {
    for(unsigned int d_bin=0;d_bin<n_d;++d_bin)
    {
      float min_d_a[4] __attribute__((aligned(16))) = {min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin],min_d_array[d_bin]};
      float max_d_a[4] __attribute__((aligned(16))) = {max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin],max_d_array[d_bin]};
      
      for(unsigned int k_bin=0;k_bin<n_k;++k_bin)
      {
        float min_k_a[4] __attribute__((aligned(16))) = {min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin],min_k_array[k_bin]};
        float max_k_a[4] __attribute__((aligned(16))) = {max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin],max_k_array[k_bin]};
        if(separate_by_helicity==true)
        {
          float hel=-1.;
          if(helicity==true){hel=1.;}
          if(k_bin==0)
          {
            HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, hel, phi_3_out, phi_4_out);
            
            phi_3_in = phi_3_out;
            phi_4_in = phi_4_out;
          }
          else
          {
            HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, max_k_a, min_phi_1_a, max_phi_1_a, hel, phi_3_in, phi_4_in, phi_3_out, phi_4_out);
            phi_3_in = phi_3_out;
            phi_4_in = phi_4_out;
          }
        }
        else
        {
          HelixHough::phiRange_sse(x_a, y_a, min_d_a, max_d_a, min_k_a, max_k_a, min_phi_1_a, max_phi_1_a, min_phi_2_a, max_phi_2_a);
        }
        for(unsigned int h=0;h<hit_counter;++h)
        {
          float dphi = sqrt((four_hits[h].dx*four_hits[h].dx + four_hits[h].dy*four_hits[h].dy)/(four_hits[h].x*four_hits[h].x + four_hits[h].y*four_hits[h].y));
          dphi += phiError(four_hits[h], min_kappa, max_kappa, min_d_array[d_bin], max_d_array[d_bin], zoomranges[zoomlevel].min_z0, zoomranges[zoomlevel].max_z0, zoomranges[zoomlevel].min_dzdl, zoomranges[zoomlevel].max_dzdl);
          
          min_phi_1_a[h] -= dphi;
          min_phi_2_a[h] -= dphi;
          max_phi_1_a[h] += dphi;
          max_phi_2_a[h] += dphi;
        }
        if(separate_by_helicity==true)
        {
          fillBins(total_bins, hit_counter, min_phi_1_a, max_phi_1_a, four_hits, z_bins,  n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
        }
        else
        {
          fillBins(total_bins, hit_counter, min_phi_1_a, max_phi_1_a, four_hits, z_bins,  n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
          fillBins(total_bins, hit_counter, min_phi_2_a, max_phi_2_a, four_hits, z_bins, n_d, n_k, n_dzdl, n_z0, d_bin, k_bin, n_phi, zoomlevel, low_phi, high_phi, inv_phi_range, vote_array);
        }
      }
    }
    hit_counter = 0;
  }
  gettimeofday(&t2, NULL);
  time1 = ((double)(t1.tv_sec) + (double)(t1.tv_usec)/1000000.);
  time2 = ((double)(t2.tv_sec) + (double)(t2.tv_usec)/1000000.);
  xy_vote_time += (time2 - time1);
  if(vote_array.size == 0){return;}
  if(vote_array.size < (1<<14))
  {
    if(vote_array.size > total_bins)
    {
      unsigned int B[1<<14];
      unsigned int C[(1<<12)+1] = {0};
      counting_sort(vote_array.size, total_bins, vote_array.arr, B, C);
    }
    else
    {
      unsigned int B[1<<14];
      radix_sort(vote_array.arr, B, vote_array.size);
    }
    bins_vec[zoomlevel]->resize(vote_array.size, BinEntryPair5D(0,0));
    for(unsigned int i=0;i<vote_array.size;++i)
    {
      (*(bins_vec[zoomlevel]))[i].bin = (vote_array.arr[i])>>20;
      (*(bins_vec[zoomlevel]))[i].entry = (vote_array.arr[i])&((unsigned int)1048575);
      (*(bins_vec[zoomlevel]))[i].is_seed = false;
    }
  }
  else
  {
    bins_vec[zoomlevel]->resize(vote_array.size, BinEntryPair5D(0,0));
    for(unsigned int i=0;i<vote_array.size;++i)
    {
      (*(bins_vec[zoomlevel]))[i].bin = (vote_array[i])>>20;
      (*(bins_vec[zoomlevel]))[i].entry = (vote_array[i])&((unsigned int)1048575);
      (*(bins_vec[zoomlevel]))[i].is_seed = false;
    }
    if(bins_vec[zoomlevel]->size() > total_bins){counting_sort(total_bins, *(bins_vec[zoomlevel]));}
    else if(total_bins<128){radix_sort(*(bins_vec[zoomlevel]));}
    else{sort(bins_vec[zoomlevel]->begin(), bins_vec[zoomlevel]->end());}
  }
}
