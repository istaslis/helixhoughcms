#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TNtuple.h"
#include <cmath>
#include <algorithm>
#include <map>
#include <sstream>
#include "TrackingEvaluator.h"
#include "SimpleTrack.h"
#include "DetectorGeometry.h"
#include "TCanvas.h"


using namespace std;


class floatBin
{
  public:
    floatBin(float l, float h) : low(l), high(h) {}
    ~floatBin(){}
    float low;
    float high;
    bool operator <(const floatBin& other) const
    {
      return ( high < other.low );
    }

};


//return which bin in bins that pt belongs to, or -1 if it doesn't belong to any
static int pt_bin(vector<floatBin>& bins, float pt)
{
  floatBin bin(pt,pt);
  if( ( bin < bins[0] ) || ( bins.back() < bin ) )
  {
	  cout << "wrong bin: {"<<bin.low<<", "<<bin.high<<"}"<<endl;
	  cout << "min bin: {"<<bins[0].low<<", "<<bins[0].high<<"}"<<endl;
	  cout << "max bin: {"<<bins.back().low<<", "<<bins.back().high<<"}"<<endl;


	  return -1;
  }
  pair<vector<floatBin>::iterator,vector<floatBin>::iterator> bounds;
  bounds = equal_range(bins.begin(), bins.end(), bin);
  return ( (int)(bounds.first - bins.begin()) );
}


// pT in GeV/c , B in T , kappa in cm^-1
static inline double pT_to_kappa(double B, double pT)
{
  return 0.003*B/pT;
}


static inline double kappa_to_pT(double B, double kappa)
{
  return 0.003*B/kappa;
}

string AppendToFilename(string s, unsigned int n1, unsigned int n2)
{
	stringstream ss;
	ss.str("");
	ss<<s<<n1<<"_"<<n2<<"_CMS.png";
	return ss.str();
}

// calculate_eff <input file> <histogram output file>
int main(int argc, char** argv)
{
  cout << "calculate_eff_CMS: : Press enter to continue..."<<endl;
  cin.get();

  DetectorGeometry geometry;
  geometry.ReadFromFile(string(argv[3]));
  double B = geometry.detector_B_field;

  TFile mc_file(argv[1]);
  TTree* mc_tree = 0;
  mc_file.GetObject("mc_events", mc_tree);
  SimpleMCEvent* mc_event=0;
  mc_tree->SetBranchAddress("tracks", &mc_event);

  //find min and max pt to organize bins:
  float pTmin=(uint)-1, pTmax=0;
  for(int ev=0;ev<mc_tree->GetEntries();++ev)
  {
    mc_file.cd();
    mc_tree->GetEntry(ev);

    for(unsigned int i=0;i<mc_event->tracks.size();++i)
    {
      float pt = kappa_to_pT(B, mc_event->tracks[i].kappa);
      if (pt>pTmax) pTmax = pt;
      if (pt<pTmin) pTmin = pt;
    }
  }

  cout << "Min : "<<pTmin<<", Max : "<<pTmax<<endl;

  //include more in bins, because reco pT range can go outside of MC pT range
  float width = pTmax - pTmin;
  pTmin =pTmin-width/2 > 0?pTmin-width/2 :0;
  pTmax+=width/2;

  //define pt bins to use
  const unsigned int bins = 27;

  float xbins[bins+1];
  float binWidth = (pTmax - pTmin)/(bins-2); //add one bin tho the left and to the right
  pTmin-=binWidth;
  vector<floatBin> pt_bins;
  for(int i=0;i<bins;++i)
  {
    pt_bins.push_back( floatBin(((float)(i))*binWidth + pTmin,((float)(i+1))*binWidth + pTmin) );
    xbins[i] =((float)(i))*binWidth + pTmin;
  }

  xbins[bins] = xbins[bins-1]+binWidth;

  TH1D n_mc("n_mc", "# mc tracks vs pt", pt_bins.size(), xbins);
  TH1D n_reco("n_reco", "# reco tracks vs pt", pt_bins.size(), xbins);
  TH1D n_fake("n_fake", "# fake tracks vs pt", pt_bins.size(), xbins);
  TH1D efficiency("efficiency", "efficiency vs pt", pt_bins.size(), xbins);
  TH1D fake_rate("fake_rate", "fake rate vs pt", pt_bins.size(), xbins);
  TH1D mom_res("mom_res", "momentum resolution vs pt", pt_bins.size(), xbins);
  

  TNtuple singleMatch("singleMatch", "single match track numbers vs pt", "tracks", 100000 );
  TNtuple multiMatch("multiMatch", "multiple match track numbers vs pt", "tracks", 100000 );//pt_bins.size(), xbins);

  stringstream ss;
  
  vector<TH1D*> momresbins;
  for(unsigned int i=0;i<pt_bins.size();++i)
  {
    ss.clear();ss.str("");
    ss<<"momresbin_"<<i;
    momresbins.push_back( new TH1D(ss.str().c_str(), ss.str().c_str(), 1024, -1., 1.) );
  }
  
  TH1D purity_4("purity_4", "proportion of tracks with 4 correct hits", pt_bins.size(), xbins);
  TH1D purity_5("purity_5", "proportion of tracks with 4 or 5 correct hits", pt_bins.size(), xbins);
  TH1D purity_6("purity_6", "proportion of tracks with 4, 5, or 6 correct hits", pt_bins.size(), xbins);
  vector<unsigned int> n_4;n_4.assign(pt_bins.size(), 0);
  vector<unsigned int> n_5;n_5.assign(pt_bins.size(), 0);
  vector<unsigned int> n_6;n_6.assign(pt_bins.size(), 0);
  



  TFile reco_file(argv[1]);
  TTree* reco_tree = 0;
  reco_file.GetObject("reco_events", reco_tree);
  SimpleRecoEvent* reco_event=0;
  reco_tree->SetBranchAddress("tracks", &reco_event);
  
  vector<vector<vector<unsigned int> > > mctracks;//pt bin, mc track index, hit index
  mctracks.assign(pt_bins.size(), vector<vector<unsigned int> >());
  vector<vector<float> > mc_mom;
  mc_mom.assign(pt_bins.size(), vector<float>());
  
  vector<vector<unsigned int> > recotracks;//reco track index, hit index
  vector<float> reco_mom;
  
  float avgseedtime = 0., avgprimtime = 0.;
  unsigned int n_events = 0, n_tracks = 0;
  n_events = mc_tree->GetEntries();

  for(int ev=0;ev<mc_tree->GetEntries();++ev)
  {
    mc_file.cd();
    mc_tree->GetEntry(ev);

    if (ev==0) n_tracks = mc_event->tracks.size();//assumed the same # of tracks in all events

    for(unsigned int i=0;i<mctracks.size();++i)
    {
      mctracks[i].clear();
      mc_mom[i].clear();
    }
    for(unsigned int i=0;i<mc_event->tracks.size();++i)
    {
      float pt = kappa_to_pT(B, mc_event->tracks[i].kappa);
      int bin = pt_bin(pt_bins, pt);
      if(bin < 0){continue;}
      mctracks[bin].push_back(vector<unsigned int>());
      mc_mom[bin].push_back(kappa_to_pT(B, mc_event->tracks[i].kappa));
      for(unsigned int h=0;h<mc_event->tracks[i].hits.size();++h)
      {
        mctracks[bin].back().push_back(mc_event->tracks[i].hits[h].index);
      }
    }
    
    reco_file.cd();
    reco_tree->GetEntry(ev);

    avgseedtime+=reco_event->seedTrackingTime;
    avgprimtime+=reco_event->primaryTrackingTime;

    
    recotracks.clear();
    reco_mom.clear();
    
    for(unsigned int i=0;i<reco_event->tracks.size();++i)
    {
      recotracks.push_back(vector<unsigned int>());
      reco_mom.push_back(kappa_to_pT(B, reco_event->tracks[i].kappa));
      for(unsigned int h=0;h<reco_event->tracks[i].indexes.size();++h)
      {
        recotracks.back().push_back(reco_event->tracks[i].indexes[h]);
      }

    }

    
    vector<vector<unsigned int> > mc_is_reconstructed;   mc_is_reconstructed.assign(pt_bins.size(), vector<unsigned int>());
    vector<bool> reco_used;reco_used.assign(recotracks.size(), false);
    
    unsigned int nrecototal = 0;

    for(unsigned int p=0;p<pt_bins.size();++p)
    {
      map<unsigned int, unsigned int> contr;
      unsigned int nreco = 0;
      vector<double> momresv;
      calculateEfficiency(mctracks[p], recotracks, 10, nreco, mc_is_reconstructed[p], reco_used, mc_mom[p], reco_mom,momresv , contr);

      //copy momentum residual values into histogram
      for_each(momresv.begin(), momresv.end(),[=](double v){momresbins[p]->Fill(v);});
      
      map<unsigned int, unsigned int>::iterator it;
      it = contr.find(12);
      if(it != contr.end()){n_4[p] += it->second;}
      it = contr.find(13);
      if(it != contr.end()){n_5[p] += it->second;}
      it = contr.find(6);
      //if(it != contr.end()){
	n_6[p] += it->second;//}

	  nrecototal+=nreco;
      
      n_mc.SetBinContent(p+1, n_mc.GetBinContent(p+1) + mctracks[p].size());
      n_reco.SetBinContent(p+1, n_reco.GetBinContent(p+1) + nreco);

      //get single and multi match #
      unsigned int single = 0, multi = 0;
      for (uint i = 0; i<mc_is_reconstructed[p].size();++i)
    	  if (mc_is_reconstructed[p][i]==1) singleMatch.Fill(xbins[p]);
    		  //single++;
    	  else if (mc_is_reconstructed[p][i]>1)
    		  //for (int k = 0; k<mc_is_reconstructed[p][i];k++)
    			  multiMatch.Fill(xbins[p]);
    		  //multi++;

      //singleMatch.SetBinContent(p+1, singleMatch.GetBinContent(p+1) + single);

      //multiMatch.SetBinContent(p+1, multiMatch.GetBinContent(p+1) + multi);
    }

    //cout<<"mc_is_reconstructed.size() : "<<mc_is_reconstructed.size()<<endl;

    //for (uint p=0; p<mc_is_reconstructed.size();++p)
    //	for (uint i=0;i<mc_is_reconstructed[p].size();++i)
    //	cout<<"MC track "<<i<<" in pbin "<<p<<" was hit times: "<<mc_is_reconstructed[p][i]<<endl;




    for(unsigned int i=0;i<reco_used.size();++i)
    {
    	 float pt = kappa_to_pT(B, reco_event->tracks[i].kappa);
      if(reco_used[i] == false)
      {

        int bin = pt_bin(pt_bins, pt);
        if(bin < 0){continue;}
        n_fake.SetBinContent(bin+1, n_fake.GetBinContent(bin+1) + 1);
      }
    }

    cout<<"Total mc tracks reconstructed: "<<nrecototal<<endl;

  }
  
  avgseedtime/=mc_tree->GetEntries();
  avgprimtime/=mc_tree->GetEntries();
  cout << "Average seeding  time: "<<avgseedtime<<endl;
  cout << "Average tracking time: "<<avgprimtime<<endl;

  for(unsigned int i=0;i<pt_bins.size();++i)
  {
    mom_res.SetBinContent(i+1, momresbins[i]->GetRMS());
  }
  
  n_mc.Sumw2();
  n_reco.Sumw2();
  n_fake.Sumw2();
  
  efficiency.Divide(&n_reco, &n_mc, 1., 1., "B");
  fake_rate.Divide(&n_fake, &n_mc);
  
  for(unsigned int p=0;p<pt_bins.size();++p)
  {
    //cout<<p<<" "<<n_4[p]<<" "<<n_5[p]<<" "<<n_6[p]<<endl;
    unsigned int ntot = n_4[p] + n_5[p] + n_6[p];
    if(ntot != 0)
    {
      purity_4.SetBinContent(p+1, ((float)(n_4[p]))/((float)(ntot)));
      purity_5.SetBinContent(p+1, ((float)(n_4[p] + n_5[p]))/((float)(ntot)));
      purity_6.SetBinContent(p+1, 1.);
    }
    else
    {
      purity_4.SetBinContent(p+1, 0.);
      purity_5.SetBinContent(p+1, 0.);
      purity_6.SetBinContent(p+1, 0.);
    }
  }
  
  TFile out(argv[2], "recreate");
  out.cd();
  n_mc.Write();
  n_reco.Write();
  n_fake.Write();
  efficiency.Write();
  fake_rate.Write();
  mom_res.Write();
  purity_4.Write();
  purity_5.Write();
  purity_6.Write();
  singleMatch.Write();
  multiMatch.Write();

  TCanvas *c = new TCanvas;
  fake_rate.Draw();
  c->SaveAs(AppendToFilename("FAKERATE",n_events,n_tracks).c_str()); c->Clear();
  n_fake.Draw();
  c->SaveAs(AppendToFilename("FAKES",n_events,n_tracks).c_str()); c->Clear();
  efficiency.Draw();
  c->SaveAs(AppendToFilename("EFFICIENCY",n_events,n_tracks).c_str()); c->Clear();
  singleMatch.Draw("tracks");
  c->SaveAs(AppendToFilename("SINGLEMATCH",n_events,n_tracks).c_str()); c->Clear();
  multiMatch.Draw("tracks");
  c->SaveAs(AppendToFilename("MULTIMATCH",n_events,n_tracks).c_str()); c->Clear();

  delete c;
  
  return 0;
}


