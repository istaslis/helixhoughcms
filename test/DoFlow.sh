cd ../fastTrackMC
./particle_gun $1 $2 PG$1_$2_CMS.txt 
./fastTrackMC PG$1_$2_CMS.txt ../Tracker/detectorCMS.txt MC$1_$2_CMS.root $3
cd ../test/
./test_CMS ../fastTrackMC/MC$1_$2_CMS.root RECO$1_$2_CMS.root ../Tracker/detectorCMS.txt
./calculate_eff_CMS RECO$1_$2_CMS.root EFF$1_$2_CMS.root ../Tracker/detectorCMS.txt
