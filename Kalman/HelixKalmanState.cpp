#include "HelixKalmanState.h"

using namespace Eigen;


HelixKalmanState::HelixKalmanState() : position(0), phi(0.), d(0.), kappa(0.), z0(0.), dzdl(0.), x_int(0.), y_int(0.), z_int(0.), chi2(0.), C(MatrixXf::Identity(5,5))
{
  
}


HelixKalmanState::~HelixKalmanState()
{
  
}
