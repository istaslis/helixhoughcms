#ifndef __HELIX_HOUGH_H__
#define __HELIX_HOUGH_H__

#include <cmath>
#include <vector>
#include <set>
#include <map>
#include "fastvec.h"
#include "HelixRange.h"
#include "HelixResolution.h"
#include "SimpleHit3D.h"
#include "SimpleTrack3D.h"
#include <xmmintrin.h>
#include <emmintrin.h>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif





class ParRange
{
public:
  ParRange(){}
  ParRange(unsigned int min_cAngle, unsigned int max_cAngle, unsigned int min_dca, unsigned int max_dca, unsigned int min_invR, unsigned int max_invR, unsigned int min_theta, unsigned int max_theta, unsigned int min_z, unsigned int max_z) : min_k(min_invR), max_k(max_invR), min_phi(min_cAngle), max_phi(max_cAngle), min_d(min_dca), max_d(max_dca), min_dzdl(min_theta), max_dzdl(max_theta), min_z0(min_z), max_z0(max_z) {}
  ~ParRange() {}
  
  void mergeRange(unsigned int phi, unsigned int d, unsigned int k, unsigned int dzdl, unsigned int z0)
  {
    min_phi = (((phi < min_phi)-1)&min_phi) ^ (((phi >= min_phi)-1)&phi);
    max_phi = (((phi > max_phi)-1)&max_phi) ^ (((phi <= max_phi)-1)&phi);
    min_d = (((d < min_d)-1)&min_d) ^ (((d >= min_d)-1)&d);
    max_d = (((d > max_d)-1)&max_d) ^ (((d <= max_d)-1)&d);
    min_k = (((k < min_k)-1)&min_k) ^ (((k >= min_k)-1)&k);
    max_k = (((k > max_k)-1)&max_k) ^ (((k <= max_k)-1)&k);
    min_dzdl = (((dzdl < min_dzdl)-1)&min_dzdl) ^ (((dzdl >= min_dzdl)-1)&dzdl);
    max_dzdl = (((dzdl > max_dzdl)-1)&max_dzdl) ^ (((dzdl <= max_dzdl)-1)&dzdl);
    min_z0 = (((z0 < min_z0)-1)&min_z0) ^ (((z0 >= min_z0)-1)&z0);
    max_z0 = (((z0 > max_z0)-1)&max_z0) ^ (((z0 <= max_z0)-1)&z0);
  }
  
  void mergeRange(ParRange& other)
  {
    min_phi = (((other.min_phi < min_phi)-1)&min_phi) ^ (((other.min_phi >= min_phi)-1)&other.min_phi);
    max_phi = (((other.max_phi > max_phi)-1)&max_phi) ^ (((other.max_phi <= max_phi)-1)&other.max_phi);
    min_d = (((other.min_d < min_d)-1)&min_d) ^ (((other.min_d >= min_d)-1)&other.min_d);
    max_d = (((other.max_d > max_d)-1)&max_d) ^ (((other.max_d <= max_d)-1)&other.max_d);
    min_k = (((other.min_k < min_k)-1)&min_k) ^ (((other.min_k >= min_k)-1)&other.min_k);
    max_k = (((other.max_k > max_k)-1)&max_k) ^ (((other.max_k <= max_k)-1)&other.max_k);
    min_dzdl = (((other.min_dzdl < min_dzdl)-1)&min_dzdl) ^ (((other.min_dzdl >= min_dzdl)-1)&other.min_dzdl);
    max_dzdl = (((other.max_dzdl > max_dzdl)-1)&max_dzdl) ^ (((other.max_dzdl <= max_dzdl)-1)&other.max_dzdl);
    min_z0 = (((other.min_z0 < min_z0)-1)&min_z0) ^ (((other.min_z0 >= min_z0)-1)&other.min_z0);
    max_z0 = (((other.max_z0 > max_z0)-1)&max_z0) ^ (((other.max_z0 <= max_z0)-1)&other.max_z0);
  }
  
  unsigned int min_k, max_k;
  unsigned int min_phi, max_phi;
  unsigned int min_d, max_d;
  unsigned int min_dzdl, max_dzdl;
  unsigned int min_z0, max_z0;
};


class ParameterCluster
{
  public:
    ParameterCluster(){}
    ~ParameterCluster(){}
    
    ParRange range;
    std::vector<unsigned int> bins_list;
    std::vector<unsigned int> hit_indexes;
};

class BinEntryPair5D
{
public:
  BinEntryPair5D(unsigned int b=0, unsigned int e=0, bool is=false) : bin(b), entry(e), is_seed(is) {}
  ~BinEntryPair5D(){}
  
  bool operator<(const BinEntryPair5D& other) const
  {
    return ( bin < other.bin );
  }
  
  static unsigned int linearBin(unsigned int nbins2, unsigned int nbins3, unsigned int nbins4, unsigned int nbins5, unsigned int bin1, unsigned int bin2, unsigned int bin3, unsigned int bin4, unsigned int bin5)
  {
    return bin5 + nbins5*(bin4 + nbins4*(bin3 + nbins3*( bin2 + nbins2*bin1 )));
  }
  
  void bin5D(unsigned int nbins2, unsigned int nbins3, unsigned int nbins4, unsigned int nbins5, unsigned int& bin1, unsigned int& bin2, unsigned int& bin3, unsigned int& bin4, unsigned int& bin5) const
  {
    unsigned int temp1 = nbins2*nbins3*nbins4*nbins5;
    bin1 = bin/temp1;
    unsigned int temp2 = bin - bin1*temp1;
    temp1 = nbins3*nbins4*nbins5;
    bin2 = temp2/temp1;
    temp2 -= bin2*temp1;
    temp1 = nbins4*nbins5;
    bin3 = temp2/temp1;
    temp2 -= bin3*temp1;
    temp1 = nbins5;
    bin4 = temp2/temp1;
    temp2 -= bin4*temp1;
    bin5 = temp2;
  }
  
  unsigned int bin;
  unsigned int entry;
  bool is_seed;
};



class HelixHough
{
  typedef std::vector<std::vector<std::vector<std::vector<std::vector<unsigned int> > > > > nhits_array;
  public:
    HelixHough(unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, HelixResolution& min_resolution, HelixResolution& max_resolution, HelixRange& range);
    HelixHough(std::vector<std::vector<unsigned int> >& zoom_profile, unsigned int minzoom, HelixRange& range);
    virtual ~HelixHough();
    
    void initNhitsArray(nhits_array& array, unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0);
    void initHelixHough(unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, HelixResolution& min_resolution, HelixResolution& max_resolution, HelixRange& range);
    
    void findHelices(std::vector<SimpleHit3D>& hits, unsigned int min_hits, unsigned int max_hits, std::vector<SimpleTrack3D>& tracks, unsigned int maxtracks=0);
    
    void findHelices(unsigned int min_hits, unsigned int max_hits, std::vector<SimpleTrack3D>& tracks, unsigned int maxtracks, unsigned int zoomlevel);
    
    void findSeededHelices(std::vector<SimpleTrack3D>& seeds, std::vector<SimpleHit3D>& hits, unsigned int min_hits, unsigned int max_hits, std::vector<SimpleTrack3D>& tracks, unsigned int maxtracks=0);
    void findSeededHelices(unsigned int min_hits, unsigned int max_hits, std::vector<SimpleTrack3D>& tracks, unsigned int maxtracks, unsigned int zoomlevel);
    
    void vote(unsigned int zoomlevel);
    static void phiRange_sse(float* hit_x, float* hit_y, float* min_d, float* max_d, float* min_k, float* max_k, float* min_phi_1, float* max_phi_1, float* min_phi_2, float* max_phi_2);
    static void phiRange_sse(float* hit_x, float* hit_y, float* min_d, float* max_d, float* min_k, float* max_k, float* min_phi, float* max_phi, float hel, __m128& phi_3_out, __m128& phi_4_out);
    static void phiRange_sse(float* hit_x, float* hit_y, float* min_d, float* max_d, float* max_k, float* min_phi, float* max_phi, float hel, __m128& phi_3, __m128& phi_4, __m128& phi_3_out, __m128& phi_4_out);
    static void dzdlRange_sse(float* x_a, float* y_a, float* z_a, float cosphi1, float sinphi1, float cosphi2, float sinphi2, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float* min_dzdl_a, float* max_dzdl_a);
    static void phiRange_sse(float* hit_x, float* hit_y, float* min_d, float* max_d, float* min_k, float* max_k, float* min_phi, float* max_phi, float* min_phi_2, float* max_phi_2, float hel, __m128& phi_3_out, __m128& phi_4_out, float* hit_x_2, float* hit_y_2, __m128& phi_3_out_2, __m128& phi_4_out_2);
    static void phiRange_sse(float* hit_x, float* hit_y, float* min_d, float* max_d, float* min_k, float* max_k, float* min_phi, float* max_phi, float* min_phi_2, float* max_phi_2, float hel, __m128& phi_3, __m128& phi_4, __m128& phi_3_out, __m128& phi_4_out, float* hit_x_2, float* hit_y_2, __m128& phi_3_2, __m128& phi_4_2, __m128& phi_3_out_2, __m128& phi_4_out_2);
    
    void setPrintTimings(bool pt){print_timings=pt;}
    
    virtual void finalize(std::vector<SimpleTrack3D>& input, std::vector<SimpleTrack3D>& output){}
    virtual void findTracks(std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range) = 0;
    virtual void initEvent(std::vector<SimpleHit3D>& hits, unsigned int min_hits){}
    virtual void findSeededTracks(std::vector<SimpleTrack3D>& seeds, std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range){}
    
    // return true if we want to go straight into the user-written findTracks function instead of possibly continuing the Hough Transform
    virtual bool breakRecursion(const std::vector<SimpleHit3D>& hits, const HelixRange& range){return false;}
    
    // additional phi error to add to a hit in the voting stage, given an bin with the k and dzdl parameters passed.  This is useful 
    // to take into account multiple scattering
    virtual float phiError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl){return 0.;}
    // same as above, but for additional error on dzdl
    virtual float dzdlError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl){return 0.;}
    
    virtual void initSeeding(){}
    
    void setSeparateByHelicity(bool sbh){separate_by_helicity=sbh;}
    
    void requireLayers(unsigned int nl)
    {
      check_layers = true;
      req_layers = nl;
    }
    
  private:
    //voting array for each zoom level
    std::vector<nhits_array*> nhits_array_vec;
    //top level hits
    std::vector<SimpleHit3D>* base_hits;
    // vector of hits used for isolating hits in a single zoom level
    std::vector<std::vector<SimpleHit3D>* > hits_vec;
    // for each zoomlevel, a vector of pair indexes, with the index mapping to the entry position in hits_vec
    std::vector<std::vector<std::vector<unsigned int> >* > pairs_vec;
    // stores external index values
    std::vector<unsigned int> index_mapping;
    /// vector of BinEntryPairs which associate the bins stored in the accessor array with the actual hit.  one per zoomlevel
    std::vector<std::vector<BinEntryPair5D>* > bins_vec;
    
    std::vector<std::vector<SimpleTrack3D>* > seeds_vec;
    
    HelixRange top_range;
    HelixRange current_range;
    std::vector<HelixRange> zoomranges;
    std::vector<std::vector<ParameterCluster>*> clusters_vec;
    
    unsigned int max_zoom;//maximum number of times to zoom in on the parameter array (increase resolution)
    unsigned int min_zoom;//minimum number of times to zoom in on the parameter array (increase resolution)
    
    bool using_vertex;
    
    unsigned int max_tracks;
    
    double vote_time;
    double xy_vote_time;
    double z_vote_time;
    double cluster_time;
    
    bool print_timings;
    
    bool separate_by_helicity,helicity;
    
    bool check_layers;
    unsigned int req_layers;
    
    static void allButKappaRange_sse(float* x1_a,float* x2_a,float* y1_a,float* y2_a,float* z1_a,float* z2_a, float* min_k_a,float* max_k_a, float* min_phi_1_a,float* max_phi_1_a,float* min_phi_2_a,float* max_phi_2_a, float* min_d_1_a,float* max_d_1_a,float* min_d_2_a,float* max_d_2_a, float* min_dzdl_a,float* max_dzdl_a, float* min_z0_1_a,float* max_z0_1_a,float* min_z0_2_a,float* max_z0_2_a);
    
    
    void fillBins(unsigned int total_bins, unsigned int hit_counter, float* min_phi_a, float* max_phi_a, std::vector<SimpleHit3D>& four_hits, fastvec2d& z_bins, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, unsigned int d_bin, unsigned int k_bin, unsigned int n_phi, unsigned int zoomlevel, float low_phi, float high_phi, float inv_phi_range, fastvec& vote_array);
    
    void makeClusters(unsigned int zoomlevel, unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, unsigned int min_hits, std::vector<ParameterCluster>& clusters, bool& use_clusters, bool& is_super_bin);
    
    void vote_z(unsigned int zoomlevel, unsigned int n_phi, unsigned int n_d, unsigned int
    n_k, unsigned int n_dzdl, unsigned int n_z0, fastvec2d& z_bins);
};

#endif

