#include "vector_math_inline.h"
#include "Tracker.h"
#include <cmath>
#include <iostream>
#include <algorithm>
#include <Eigen/LU>
#include <Eigen/Core>
#include <Eigen/Dense>


using namespace std;
using namespace Eigen;


void Tracker::calculateKappaTangents(float* x1_a, float* y1_a, float* z1_a, float* x2_a, float* y2_a, float* z2_a, float* x3_a, float* y3_a, float* z3_a, float* dx1_a, float* dy1_a, float* dz1_a, float* dx2_a, float* dy2_a, float* dz2_a, float* dx3_a, float* dy3_a, float* dz3_a, float* kappa_a, float* dkappa_a, float* ux_mid_a, float* uy_mid_a, float* ux_end_a, float* uy_end_a, float* dzdl_1_a, float* dzdl_2_a, float* ddzdl_1_a, float* ddzdl_2_a)
{
  static const __m128 two = {2., 2., 2., 2.};

  __m128 x1 = _mm_load_ps(x1_a);
  __m128 x2 = _mm_load_ps(x2_a);
  __m128 x3 = _mm_load_ps(x3_a);
  __m128 y1 = _mm_load_ps(y1_a);
  __m128 y2 = _mm_load_ps(y2_a);
  __m128 y3 = _mm_load_ps(y3_a);
  __m128 z1 = _mm_load_ps(z1_a);
  __m128 z2 = _mm_load_ps(z2_a);
  __m128 z3 = _mm_load_ps(z3_a);

  __m128 dx1 = _mm_load_ps(dx1_a);
  __m128 dx2 = _mm_load_ps(dx2_a);
  __m128 dx3 = _mm_load_ps(dx3_a);
  __m128 dy1 = _mm_load_ps(dy1_a);
  __m128 dy2 = _mm_load_ps(dy2_a);
  __m128 dy3 = _mm_load_ps(dy3_a);
  __m128 dz1 = _mm_load_ps(dz1_a);
  __m128 dz2 = _mm_load_ps(dz2_a);
  __m128 dz3 = _mm_load_ps(dz3_a);


  __m128 D12 = _mm_sub_ps(x2, x1);
  D12 = _mm_mul_ps(D12, D12);
  __m128 tmp1 = _mm_sub_ps(y2, y1);
  tmp1 = _mm_mul_ps(tmp1, tmp1);
  D12 = _mm_add_ps(D12, tmp1);
  D12 = _vec_sqrt_ps(D12);

  __m128 D23 = _mm_sub_ps(x3, x2);
  D23 = _mm_mul_ps(D23, D23);
  tmp1 = _mm_sub_ps(y3, y2);
  tmp1 = _mm_mul_ps(tmp1, tmp1);
  D23 = _mm_add_ps(D23, tmp1);
  D23 = _vec_sqrt_ps(D23);

  __m128 D31 = _mm_sub_ps(x1, x3);
  D31 = _mm_mul_ps(D31, D31);
  tmp1 = _mm_sub_ps(y1, y3);
  tmp1 = _mm_mul_ps(tmp1, tmp1);
  D31 = _mm_add_ps(D31, tmp1);
  D31 = _vec_sqrt_ps(D31);

  __m128 k = _mm_mul_ps(D12, D23);
  k = _mm_mul_ps(k, D31);
  k = _vec_rec_ps(k);
  tmp1 = (D12 + D23 + D31)*(D23 + D31 - D12)*(D12 + D31 - D23)*(D12 + D23 - D31);
  tmp1 = _vec_sqrt_ps(tmp1);
  k *= tmp1;

  __m128 tmp2 = _mm_cmpgt_ps(tmp1, zero);
  tmp1 = _mm_and_ps(tmp2, k);
  tmp2 = _mm_andnot_ps(tmp2, zero);
  k = _mm_xor_ps(tmp1, tmp2);

  _mm_store_ps(kappa_a, k);
  __m128 k_inv = _vec_rec_ps(k);

  __m128 D12_inv = _vec_rec_ps(D12);
  __m128 D23_inv = _vec_rec_ps(D23);
  __m128 D31_inv = _vec_rec_ps(D31);

  __m128 dr1 = dx1*dx1 + dy1*dy1;
  dr1 = _vec_sqrt_ps(dr1);
  __m128 dr2 = dx2*dx2 + dy2*dy2;
  dr2 = _vec_sqrt_ps(dr2);
  __m128 dr3 = dx3*dx3 + dy3*dy3;
  dr3 = _vec_sqrt_ps(dr3);

  __m128 dk1 = (dr1 + dr2)*D12_inv*D12_inv;
  __m128 dk2 = (dr2 + dr3)*D23_inv*D23_inv;
  __m128 dk = dk1 + dk2;
  _mm_store_ps(dkappa_a, dk);

  __m128 ux12 = (x2 - x1)*D12_inv;
  __m128 uy12 = (y2 - y1)*D12_inv;
  __m128 ux23 = (x3 - x2)*D23_inv;
  __m128 uy23 = (y3 - y2)*D23_inv;
  __m128 ux13 = (x3 - x1)*D31_inv;
  __m128 uy13 = (y3 - y1)*D31_inv;

  __m128 cosalpha = ux12*ux13 + uy12*uy13;
  __m128 sinalpha = ux13*uy12 - ux12*uy13;

  __m128 ux_mid = ux23*cosalpha - uy23*sinalpha;
  __m128 uy_mid = ux23*sinalpha + uy23*cosalpha;
  _mm_store_ps(ux_mid_a, ux_mid);
  _mm_store_ps(uy_mid_a, uy_mid);

  __m128 ux_end = ux23*cosalpha + uy23*sinalpha;
  __m128 uy_end = uy23*cosalpha - ux23*sinalpha;

  _mm_store_ps(ux_end_a, ux_end);
  _mm_store_ps(uy_end_a, uy_end);

  //asin(x) = 2*atan( x/( 1 + sqrt( 1 - x*x ) ) )
  __m128 v = one - sinalpha*sinalpha;
  v = _vec_sqrt_ps(v);
  v += one;
  v = _vec_rec_ps(v);
  v *= sinalpha;
  __m128 s2 = _vec_atan_ps(v);
  s2 *= two;
  s2 *= k_inv;
  tmp1 = _mm_cmpgt_ps(k, zero);
  tmp2 = _mm_and_ps(tmp1, s2);
  tmp1 = _mm_andnot_ps(tmp1, D23);
  s2 = _mm_xor_ps(tmp1, tmp2);

  // dz/dl = (dz/ds)/sqrt(1 + (dz/ds)^2)
  // = dz/sqrt(s^2 + dz^2)
  __m128 del_z_2 = z3 - z2;
  __m128 dzdl_2 = s2*s2 + del_z_2*del_z_2;
  dzdl_2 = _vec_rsqrt_ps(dzdl_2);
  dzdl_2 *= del_z_2;
  __m128 ddzdl_2 = (dz2 + dz3)*D23_inv;
  _mm_store_ps(dzdl_2_a, dzdl_2);
  _mm_store_ps(ddzdl_2_a, ddzdl_2);

  sinalpha = ux13*uy23 - ux23*uy13;
  v = one - sinalpha*sinalpha;
  v = _vec_sqrt_ps(v);
  v += one;
  v = _vec_rec_ps(v);
  v *= sinalpha;
  __m128 s1 = _vec_atan_ps(v);
  s1 *= two;
  s1 *= k_inv;
  tmp1 = _mm_cmpgt_ps(k, zero);
  tmp2 = _mm_and_ps(tmp1, s1);
  tmp1 = _mm_andnot_ps(tmp1, D12);
  s1 = _mm_xor_ps(tmp1, tmp2);

  __m128 del_z_1 = z2 - z1;
  __m128 dzdl_1 = s1*s1 + del_z_1*del_z_1;
  dzdl_1 = _vec_rsqrt_ps(dzdl_1);
  dzdl_1 *= del_z_1;
  __m128 ddzdl_1 = (dz1 + dz2)*D12_inv;
  _mm_store_ps(dzdl_1_a, dzdl_1);
  _mm_store_ps(ddzdl_1_a, ddzdl_1);
}


class TrackSegment
{
  public:
    TrackSegment() : chi2(0.), ux(0.), uy(0.), kappa(0.), dkappa(0.), seed(0) {}
    ~TrackSegment(){}

    float chi2;
    float ux,uy;
    float kappa;
    float dkappa;
    unsigned int hits[20];
    unsigned int seed;
};


void Tracker::findTracksBySegments(vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
  vector<TrackSegment> segments1;
  vector<TrackSegment> segments2;
  vector<TrackSegment>* cur_seg = &segments1;
  vector<TrackSegment>* next_seg = &segments2;

  vector<vector<SimpleHit3D> > layer_sorted;
  vector<SimpleHit3D> one_layer;
  layer_sorted.assign(n_layers, one_layer);
  for(unsigned int i=0;i<hits.size();++i)
  {
    layer_sorted[hits[i].layer].push_back(hits[i]);
  }
  for(unsigned int l=0;l<n_layers;++l)
  {
    if(layer_sorted[l].size()==0){return;}
  }

  float cosang_cut = 0.999;
  float cosang_diff = 1. - cosang_cut;
  float cosang_diff_inv = 1./cosang_diff;
  float sinang_cut = sqrt(1. - cosang_cut*cosang_cut);
  float easy_chi2_cut = 4.;

  unsigned int hit_counter = 0;
  float x1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float x2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float x3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float dx1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dx2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dx3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float kappa_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dkappa_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float ux_mid_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float uy_mid_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ux_end_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float uy_end_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float dzdl_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dzdl_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ddzdl_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ddzdl_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  unsigned int hit1[4];
  unsigned int hit2[4];
  unsigned int hit3[4];

  TrackSegment temp_segment;
  // make segments out of first 3 layers
  for(unsigned int i=0,sizei=layer_sorted[0].size();i<sizei;++i)
  {
    for(unsigned int j=0,sizej=layer_sorted[1].size();j<sizej;++j)
    {
      for(unsigned int k=0,sizek=layer_sorted[2].size();k<sizek;++k)
      {
        x1_a[hit_counter] = layer_sorted[0][i].x;
        y1_a[hit_counter] = layer_sorted[0][i].y;
        z1_a[hit_counter] = layer_sorted[0][i].z;
        dx1_a[hit_counter] = layer_sorted[0][i].dx;
        dy1_a[hit_counter] = layer_sorted[0][i].dy;
        dz1_a[hit_counter] = layer_sorted[0][i].dz;

        x2_a[hit_counter] = layer_sorted[1][j].x;
        y2_a[hit_counter] = layer_sorted[1][j].y;
        z2_a[hit_counter] = layer_sorted[1][j].z;
        dx2_a[hit_counter] = layer_sorted[1][j].dx;
        dy2_a[hit_counter] = layer_sorted[1][j].dy;
        dz2_a[hit_counter] = layer_sorted[1][j].dz;

        x3_a[hit_counter] = layer_sorted[2][k].x;
        y3_a[hit_counter] = layer_sorted[2][k].y;
        z3_a[hit_counter] = layer_sorted[2][k].z;
        dx3_a[hit_counter] = layer_sorted[2][k].dx;
        dy3_a[hit_counter] = layer_sorted[2][k].dy;
        dz3_a[hit_counter] = layer_sorted[2][k].dz;

        hit1[hit_counter] = i;
        hit2[hit_counter] = j;
        hit3[hit_counter] = k;

        hit_counter += 1;

        if(hit_counter == 4)
        {
          calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

          for(unsigned int h=0;h<hit_counter;++h)
          {
            temp_segment.chi2 = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
            temp_segment.chi2 *= temp_segment.chi2;
            if(temp_segment.chi2 > 2.0){continue;}
            temp_segment.ux = ux_end_a[h];
            temp_segment.uy = uy_end_a[h];
            temp_segment.kappa = kappa_a[h];
            temp_segment.dkappa = dkappa_a[h];
            temp_segment.hits[0] = hit1[h];
            temp_segment.hits[1] = hit2[h];
            temp_segment.hits[2] = hit3[h];
            next_seg->push_back(temp_segment);
          }

          hit_counter=0;
        }
      }
    }
  }
  if(hit_counter != 0)
  {
    calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

    for(unsigned int h=0;h<hit_counter;++h)
    {
      temp_segment.chi2 = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
      temp_segment.chi2 *= temp_segment.chi2;
      if(temp_segment.chi2 > 2.0){continue;}
      temp_segment.ux = ux_end_a[h];
      temp_segment.uy = uy_end_a[h];
      temp_segment.kappa = kappa_a[h];
      temp_segment.dkappa = dkappa_a[h];
      temp_segment.hits[0] = hit1[h];
      temp_segment.hits[1] = hit2[h];
      temp_segment.hits[2] = hit3[h];
      next_seg->push_back(temp_segment);
    }

    hit_counter=0;
  }
  swap(cur_seg, next_seg);


  // add hits to segments layer-by-layer, cutting out bad segments
  unsigned int whichseg[4];
  for(unsigned int l=3;l<n_layers;++l)
  {
    if(l == (n_layers-1)){easy_chi2_cut*=0.75;}
    next_seg->clear();
    for(unsigned int i=0,sizei=cur_seg->size();i<sizei;++i)
    {
      for(unsigned int j=0,sizej=layer_sorted[l].size();j<sizej;++j)
      {
        x1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].x;
        y1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].y;
        z1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].z;
        x2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].x;
        y2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].y;
        z2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].z;
        x3_a[hit_counter] = layer_sorted[l][j].x;
        y3_a[hit_counter] = layer_sorted[l][j].y;
        z3_a[hit_counter] = layer_sorted[l][j].z;

        dx1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].dx;
        dy1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].dy;
        dz1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[i].hits[l-2]].dz;
        dx2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].dx;
        dy2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].dy;
        dz2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[i].hits[l-1]].dz;
        dx3_a[hit_counter] = layer_sorted[l][j].dx;
        dy3_a[hit_counter] = layer_sorted[l][j].dy;
        dz3_a[hit_counter] = layer_sorted[l][j].dz;

        whichseg[hit_counter] = i;
        hit1[hit_counter] = j;

        hit_counter += 1;
        if(hit_counter == 4)
        {
          calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

          for(unsigned int h=0;h<hit_counter;++h)
          {
            float kdiff = (*cur_seg)[whichseg[h]].kappa - kappa_a[h];
            float dk = (*cur_seg)[whichseg[h]].dkappa + dkappa_a[h];
            dk += sinang_cut*kappa_a[h];
            float chi2_k = kdiff*kdiff/(dk*dk);
            float cos_scatter = (*cur_seg)[whichseg[h]].ux*ux_mid_a[h] + (*cur_seg)[whichseg[h]].uy*uy_mid_a[h];
            float chi2_ang = (1.-cos_scatter)*(1.-cos_scatter)*cosang_diff_inv*cosang_diff_inv;
            float chi2_dzdl = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
            chi2_dzdl *= chi2_dzdl;
            if( ((*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl)/((float)l - 2.) < easy_chi2_cut )
            {
              temp_segment.chi2 = (*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl;
              temp_segment.ux = ux_end_a[h];
              temp_segment.uy = uy_end_a[h];
              temp_segment.kappa = kappa_a[h];
              temp_segment.dkappa = dkappa_a[h];
              for(unsigned int ll=0;ll<l;++ll){temp_segment.hits[ll] = (*cur_seg)[whichseg[h]].hits[ll];}
              temp_segment.hits[l] = hit1[h];
              next_seg->push_back(temp_segment);
            }
          }
          hit_counter=0;
        }
      }
    }
    if(hit_counter != 0)
    {
      calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

      for(unsigned int h=0;h<hit_counter;++h)
      {
        float kdiff = (*cur_seg)[whichseg[h]].kappa - kappa_a[h];
        float dk = (*cur_seg)[whichseg[h]].dkappa + dkappa_a[h];
        dk += sinang_cut*kappa_a[h];
        float chi2_k = kdiff*kdiff/(dk*dk);
        float cos_scatter = (*cur_seg)[whichseg[h]].ux*ux_mid_a[h] + (*cur_seg)[whichseg[h]].uy*uy_mid_a[h];
        float chi2_ang = (1.-cos_scatter)*(1.-cos_scatter)*cosang_diff_inv*cosang_diff_inv;
        float chi2_dzdl = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
        chi2_dzdl *= chi2_dzdl;
        if( ((*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k)/((float)l - 2.) < easy_chi2_cut )
        {
          temp_segment.chi2 = (*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl;
          temp_segment.ux = ux_end_a[h];
          temp_segment.uy = uy_end_a[h];
          temp_segment.kappa = kappa_a[h];
          temp_segment.dkappa = dkappa_a[h];
          for(unsigned int ll=0;ll<l;++ll){temp_segment.hits[ll] = (*cur_seg)[whichseg[h]].hits[ll];}
          temp_segment.hits[l] = hit1[h];
          next_seg->push_back(temp_segment);
        }
      }
      hit_counter=0;
    }
    swap(cur_seg, next_seg);
  }

  SimpleTrack3D temp_track;
  temp_track.hits.assign(n_layers, SimpleHit3D());
  SimpleTrack3D temp_track_3hits;
  temp_track_3hits.hits.assign(3, SimpleHit3D());
  vector<unsigned int> tempcomb;
  tempcomb.assign(n_layers,0);
  for(unsigned int i=0,sizei=cur_seg->size();i<sizei;++i)
  {
    for(unsigned int l=0;l<n_layers;++l)
    {
      tempcomb[l] = layer_sorted[l][(*cur_seg)[i].hits[l]].index;
    }
    sort(tempcomb.begin(),tempcomb.end());
    set<vector<unsigned int> >::iterator it = combos.find(tempcomb);
    if(it != combos.end()){continue;}
    combos.insert(tempcomb);
    for(unsigned int l=0;l<n_layers;++l)
    {
      temp_track.hits[l] = layer_sorted[l][(*cur_seg)[i].hits[l]];
    }

    HelixKalmanState state;
    for(unsigned int j=0;j<3;++j){temp_track_3hits.hits[j] = temp_track.hits[j];}
    fitTrack_3(temp_track_3hits);
    state.C = Matrix<float,5,5>::Zero(5,5);
    state.C(0,0) = 0.1;
    state.C(1,1) = 0.1;
    state.C(2,2) = 0.01;
    state.C(3,3) = 0.1;
    state.C(4,4) = 0.1;
    state.C *= 10.;
    state.phi = temp_track_3hits.phi;
    if(state.phi < 0.){state.phi += 2.*M_PI;}
    state.d = temp_track_3hits.d;
    state.kappa = temp_track_3hits.kappa;
    state.z0 = temp_track_3hits.z0;
    state.dzdl = temp_track_3hits.dzdl;
    bool goodtrack = true;
    for(unsigned int h=0;h<temp_track.hits.size();++h)
    {
      kalman->addHit(temp_track.hits[h], state);nfits+=1;
      if(h >= 2)
      {
        if(state.chi2 != state.chi2)
        {
          goodtrack=false;break;
        }
        if(state.chi2/(2.*((float)(h+1)) - 5.) > chi2_cut)
        {
          goodtrack=false;break;
        }
      }
    }

    temp_track.phi = state.phi;
    if(temp_track.phi < 0.){temp_track.phi += 2.*M_PI;}
    if(temp_track.phi > 2.*M_PI){temp_track.phi -= 2.*M_PI;}
    temp_track.d = state.d;
    temp_track.kappa = state.kappa;
    temp_track.z0 = state.z0;
    temp_track.dzdl = state.dzdl;

    if(goodtrack==false){continue;}
    tracks.push_back(temp_track);
    track_states.push_back(state);
  }
}


void Tracker::findSeededTracksbySegments(vector<SimpleTrack3D>& seeds, vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
	if (seeds.size() == 0) return;

  vector<TrackSegment> segments1;
  vector<TrackSegment> segments2;
  vector<TrackSegment>* cur_seg = &segments1;
  vector<TrackSegment>* next_seg = &segments2;

  vector<vector<SimpleHit3D> > layer_sorted;
  vector<SimpleHit3D> one_layer;
  layer_sorted.assign(n_layers, one_layer);
  for(unsigned int i=0;i<hits.size();++i)
  {
    layer_sorted[hits[i].layer].push_back(hits[i]);
  }
  for(unsigned int i=0;i<seeds.size();++i)
  {
    for(unsigned int h=0;h<seeds[i].hits.size();++h)
    {
      layer_sorted[seeds[i].hits[h].layer].push_back(seeds[i].hits[h]);
    }
  }

  float cosang_cut = 0.999;
  float cosang_diff = 1. - cosang_cut;
  float cosang_diff_inv = 1./cosang_diff;
  float sinang_cut = sqrt(1. - cosang_cut*cosang_cut);
  float easy_chi2_cut = 4.;

  unsigned int hit_counter = 0;
  float x1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float x2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float x3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float y3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float z3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float dx1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dx2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dx3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dy3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dz3_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float kappa_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dkappa_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float ux_mid_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float uy_mid_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ux_end_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float uy_end_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  float dzdl_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float dzdl_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ddzdl_1_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};
  float ddzdl_2_a[4] __attribute__((aligned(16))) = {0.,0.,0.,0.};

  TrackSegment temp_segment;
  unsigned int whichhit[4];
  unsigned int whichseed[4];
  for(unsigned int seed=0,seedsize=seeds.size();seed<seedsize;++seed)
  {
    x1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].x;
    y1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].y;
    z1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].z;
    x2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].x;
    y2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].y;
    z2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].z;
    x3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].x;
    y3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].y;
    z3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].z;

    dx1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].dx;
    dy1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].dy;
    dz1_a[hit_counter] = seeds[seed].hits[n_SeedLayers-3].dz;
    dx2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].dx;
    dy2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].dy;
    dz2_a[hit_counter] = seeds[seed].hits[n_SeedLayers-2].dz;
    dx3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].dx;
    dy3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].dy;
    dz3_a[hit_counter] = seeds[seed].hits[n_SeedLayers-1].dz;

    whichseed[hit_counter] = seed;
    hit_counter += 1;
    if(hit_counter == 4)
    {
      calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

      for(unsigned int h=0;h<hit_counter;++h)
      {
        temp_segment.chi2 = 0.;
        temp_segment.ux = ux_end_a[h];
        temp_segment.uy = uy_end_a[h];
        temp_segment.kappa = kappa_a[h];
        temp_segment.dkappa = dkappa_a[h];
        temp_segment.seed = whichseed[h];
        next_seg->push_back(temp_segment);
      }

      hit_counter = 0;
    }
  }
  if(hit_counter != 0)
  {
    calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

    for(unsigned int h=0;h<hit_counter;++h)
    {
      temp_segment.chi2 = 0.;
      temp_segment.ux = ux_end_a[h];
      temp_segment.uy = uy_end_a[h];
      temp_segment.kappa = kappa_a[h];
      temp_segment.dkappa = dkappa_a[h];
      temp_segment.seed = whichseed[h];
      next_seg->push_back(temp_segment);
    }

    hit_counter = 0;
  }
  swap(cur_seg, next_seg);

  unsigned int whichseg[4];
  for(unsigned int l=n_SeedLayers;l<n_layers;++l)
  {
    next_seg->clear();
    for(unsigned int j=0,sizej=cur_seg->size();j<sizej;++j)
    {
      for(unsigned int i=0,sizei=layer_sorted[l].size();i<sizei;++i)
      {
        if((l-2)<=n_SeedLayers-1)
        {
          x1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].x;
          y1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].y;
          z1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].z;
          dx1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].dx;
          dy1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].dy;
          dz1_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-2].dz;
        }
        else
        {
          x1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].x;
          y1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].y;
          z1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].z;
          dx1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].dx;
          dy1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].dy;
          dz1_a[hit_counter] = layer_sorted[l-2][(*cur_seg)[j].hits[l-2]].dz;
        }
        if((l-1)<=n_SeedLayers-1)
        {
          x2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].x;
          y2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].y;
          z2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].z;
          dx2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].dx;
          dy2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].dy;
          dz2_a[hit_counter] = seeds[(*cur_seg)[j].seed].hits[l-1].dz;
        }
        else
        {
          x2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].x;
          y2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].y;
          z2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].z;
          dx2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].dx;
          dy2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].dy;
          dz2_a[hit_counter] = layer_sorted[l-1][(*cur_seg)[j].hits[l-1]].dz;
        }
        x3_a[hit_counter] = layer_sorted[l][i].x;
        y3_a[hit_counter] = layer_sorted[l][i].y;
        z3_a[hit_counter] = layer_sorted[l][i].z;
        dx3_a[hit_counter] = layer_sorted[l][i].dx;
        dy3_a[hit_counter] = layer_sorted[l][i].dy;
        dz3_a[hit_counter] = layer_sorted[l][i].dz;

        whichhit[hit_counter] = i;
        whichseg[hit_counter] = j;
        hit_counter += 1;

        if(hit_counter == 4)
        {
          calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

          for(unsigned int h=0;h<hit_counter;++h)
          {
            float kdiff = (*cur_seg)[whichseg[h]].kappa - kappa_a[h];
            float dk = (*cur_seg)[whichseg[h]].dkappa + dkappa_a[h];
            dk += sinang_cut*kappa_a[h];
            float chi2_k = kdiff*kdiff/(dk*dk);
            float cos_scatter = (*cur_seg)[whichseg[h]].ux*ux_mid_a[h] + (*cur_seg)[whichseg[h]].uy*uy_mid_a[h];
            float chi2_ang = (1.-cos_scatter)*(1.-cos_scatter)*cosang_diff_inv*cosang_diff_inv;
            float chi2_dzdl = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
            chi2_dzdl *= chi2_dzdl;
            if( ((*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl)/((float)l - 2.) < easy_chi2_cut )
            {
              temp_segment.chi2 = (*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl;
              temp_segment.ux = ux_end_a[h];
              temp_segment.uy = uy_end_a[h];
              temp_segment.kappa = kappa_a[h];
              temp_segment.dkappa = dkappa_a[h];
              for(unsigned int ll=0;ll<l;++ll){temp_segment.hits[ll] = (*cur_seg)[whichseg[h]].hits[ll];}
              temp_segment.hits[l] = whichhit[h];
              temp_segment.seed = (*cur_seg)[whichseg[h]].seed;
              next_seg->push_back(temp_segment);
            }
          }
          hit_counter = 0;
        }
      }
    }
    if(hit_counter != 0)
    {
      calculateKappaTangents(x1_a, y1_a, z1_a, x2_a, y2_a, z2_a, x3_a, y3_a, z3_a, dx1_a, dy1_a, dz1_a, dx2_a, dy2_a, dz2_a, dx3_a, dy3_a, dz3_a, kappa_a, dkappa_a, ux_mid_a, uy_mid_a, ux_end_a, uy_end_a, dzdl_1_a, dzdl_2_a, ddzdl_1_a, ddzdl_2_a);

      for(unsigned int h=0;h<hit_counter;++h)
      {
        float kdiff = (*cur_seg)[whichseg[h]].kappa - kappa_a[h];
        float dk = (*cur_seg)[whichseg[h]].dkappa + dkappa_a[h];
        dk += sinang_cut*kappa_a[h];
        float chi2_k = kdiff*kdiff/(dk*dk);
        float cos_scatter = (*cur_seg)[whichseg[h]].ux*ux_mid_a[h] + (*cur_seg)[whichseg[h]].uy*uy_mid_a[h];
        float chi2_ang = (1.-cos_scatter)*(1.-cos_scatter)*cosang_diff_inv*cosang_diff_inv;
        float chi2_dzdl = (dzdl_1_a[h] - dzdl_2_a[h])/(ddzdl_1_a[h] + ddzdl_2_a[h] + fabs(dzdl_1_a[h]*sinang_cut));
        chi2_dzdl *= chi2_dzdl;
        if( ((*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl)/((float)l - 2.) < easy_chi2_cut )
        {
          temp_segment.chi2 = (*cur_seg)[whichseg[h]].chi2 + chi2_ang + chi2_k + chi2_dzdl;
          temp_segment.ux = ux_end_a[h];
          temp_segment.uy = uy_end_a[h];
          temp_segment.kappa = kappa_a[h];
          temp_segment.dkappa = dkappa_a[h];
          for(unsigned int ll=0;ll<l;++ll){temp_segment.hits[ll] = (*cur_seg)[whichseg[h]].hits[ll];}
          temp_segment.hits[l] = whichhit[h];
          temp_segment.seed = (*cur_seg)[whichseg[h]].seed;
          next_seg->push_back(temp_segment);
        }
      }
      hit_counter = 0;
    }
    swap(cur_seg, next_seg);
  }

  SimpleTrack3D temp_track;
  temp_track.hits.assign(n_layers, SimpleHit3D());
  vector<unsigned int> tempcomb;
  tempcomb.assign(1+n_layers-n_SeedLayers,0);
  for(unsigned int i=0,sizei=cur_seg->size();i<sizei;++i)
  {
    tempcomb[0] = seeds[(*cur_seg)[i].seed].index;

    for(unsigned int l=n_SeedLayers;l<n_layers;++l)
    {
      tempcomb[1+l-n_SeedLayers] = layer_sorted[l][(*cur_seg)[i].hits[l]].index;
    }

    sort(tempcomb.begin(),tempcomb.end());
    set<vector<unsigned int> >::iterator it = combos.find(tempcomb);
    if(it != combos.end()){continue;}
    combos.insert(tempcomb);
    for(unsigned int l=0;l<n_SeedLayers;++l)
    {
      temp_track.hits[l] = seeds[(*cur_seg)[i].seed].hits[l];
    }
    for(unsigned int l=n_SeedLayers;l<n_layers;++l)
    {
      temp_track.hits[l] = layer_sorted[l][(*cur_seg)[i].hits[l]];
    }

    HelixKalmanState state = seed_states[seeds[(*cur_seg)[i].seed].index];
    bool goodtrack = true;
    for(unsigned int l=n_SeedLayers;l<n_layers;++l)
    {
      kalman->addHit(temp_track.hits[l], state);nfits+=1;
      if(state.chi2 != state.chi2)
      {
        goodtrack=false;break;
      }
      if(state.chi2/(2.*((float)(l+1)) - 5.) > chi2_cut)
      {
        goodtrack=false;break;
      }
    }
    temp_track.phi = state.phi;
    if(temp_track.phi < 0.){temp_track.phi += 2.*M_PI;}
    if(temp_track.phi > 2.*M_PI){temp_track.phi -= 2.*M_PI;}
    temp_track.d = state.d;
    temp_track.kappa = state.kappa;
    temp_track.z0 = state.z0;
    temp_track.dzdl = state.dzdl;

    if(goodtrack==false){continue;}
    tracks.push_back(temp_track);
    track_states.push_back(state);
  }
}



