#ifndef __EVALUATOR__
#define __EVALUATOR__

#include <vector>
#include <map>


void calculateEfficiency(std::vector<std::vector<unsigned int> >& mctracks, std::vector<std::vector<unsigned int> >& recotracks,
		unsigned int required_hits, unsigned int& nreco, std::vector<unsigned int>& mc_is_reconstructed,
		std::vector<bool>& reco_used, std::vector<float>& mc_mom, std::vector<float>& reco_mom, std::vector<double>& mom_res,
		std::map<unsigned int, unsigned int>& contributions);




#endif
