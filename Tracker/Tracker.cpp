#include "Tracker.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "vector_math_inline.h"


using namespace std;
using namespace Eigen;


class hitPair
{
public:
  hitPair(unsigned int h1, unsigned int h2, unsigned int t, float c) : hit1(h1), hit2(h2), track(t), chi2(c) {}
  ~hitPair(){}

  bool operator<(const hitPair& other) const
  {
    return ( hit1 < other.hit1 ) || ( ( hit2 < other.hit2 ) && ( hit1 == other.hit1 ) );
  }

  unsigned int hit1, hit2, track;
  float chi2;
};




Tracker::Tracker(string detectorfilename, unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, HelixResolution& min_resolution, HelixResolution& max_resolution, HelixRange& range) : HelixHough(n_phi, n_d, n_k, n_dzdl, n_z0, min_resolution, max_resolution, range) , n_layers(0),n_SeedLayers(0),chi2_cut(3.), seeding(false), verbosity(0), cut_on_dca(false), dca_cut(0.01), vertex_x(0.), vertex_y(0.), vertex_z(0.), required_layers(0), reject_ghosts(false), nfits(0), smooth_back(false), findtracksiter(0), prev_max_k(0.), prev_max_dzdl(0.), prev_p_inv(0.)
{
  use_kalman = true;

  defineDetector( detectorfilename );
}


Tracker::Tracker(string detectorfilename, vector<vector<unsigned int> >& zoom_profile, unsigned int minzoom, HelixRange& range) : HelixHough(zoom_profile, minzoom, range), n_layers(0), n_SeedLayers(0), chi2_cut(3.), seeding(false), verbosity(0), cut_on_dca(false), dca_cut(0.01), vertex_x(0.), vertex_y(0.), vertex_z(0.), required_layers(0), reject_ghosts(false), nfits(0), smooth_back(false), findtracksiter(0), prev_max_k(0.), prev_max_dzdl(0.), prev_p_inv(0.)
{
  use_kalman = true;

  defineDetector( detectorfilename );
}


Tracker::~Tracker()
{
  if ( kalman != NULL ) delete kalman;
}

void Tracker::defineDetector( string detectorfilename)
{
	/* old CMS initialization
  detector_radii.clear();

  double detector_layer_X0 = 0.;


if ( det_name == "CMS" )
  {
    setNLayers(13);
    detector_layer_X0 = 1;
    detector_B_field = 2.;

    detector_scatter.assign(n_layers, 1.41421356237309515*0.0136*sqrt( detector_layer_X0 ));
    integrated_scatter.assign(n_layers,0.);
    float total_scatter_2 = 0.;
    for(int l=0;l<n_layers;++l)
    {
      total_scatter_2 += detector_scatter[l]*detector_scatter[l];
      integrated_scatter[l] = sqrt(total_scatter_2);
    }

    detector_radii.push_back(4.4);
    detector_radii.push_back(7.3);
    detector_radii.push_back(10.2);

    detector_radii.push_back(25);
    detector_radii.push_back(34);
    detector_radii.push_back(43);
    detector_radii.push_back(52);

    detector_radii.push_back(61);
    detector_radii.push_back(69.6);
    detector_radii.push_back(78.2);
    detector_radii.push_back(86.8);
    detector_radii.push_back(96.5);
    detector_radii.push_back(108);

    detector_material.assign( n_layers, detector_layer_X0 );

    cout<<"CMS is defined, nLayers:" << n_layers<<endl;
  }
  else
  {
    cerr << "Tracker::defineDetector: Unknown detector configuration with name " << det_name << endl;
    throw 1;
  }
*/
      geometry.ReadFromFile(detectorfilename);
      setNLayers(geometry.LayersNumber);
	  kalman = new CylinderKalman(geometry.detector_radii, geometry.detector_material, geometry.detector_B_field);
}

// hel should be +- 1
static void xyTangent(SimpleHit3D& hit1, SimpleHit3D& hit2, float kappa, float hel, float& ux_out, float& uy_out, float& ux_in, float& uy_in)
{
  float x = hit2.x - hit1.x;
  float y = hit2.y - hit1.y;
  float D = sqrt(x*x + y*y);
  float ak = 0.5*kappa*D;
  float D_inv = 1./D;
  float hk = sqrt(1. - ak*ak);

  float kcx = (ak*x + hel*hk*y)*D_inv;
  float kcy = (ak*y - hel*hk*x)*D_inv;
  float ktx = -(kappa*y - kcy);
  float kty = kappa*x - kcx;
  float norm = 1./sqrt(ktx*ktx + kty*kty);
  ux_out = ktx*norm;
  uy_out = kty*norm;

  ktx = kcy;
  kty = -kcx;
  norm = 1./sqrt(ktx*ktx + kty*kty);
  ux_in = ktx*norm;
  uy_in = kty*norm;
}


// hel should be +- 1
static float cosScatter(SimpleHit3D& hit1, SimpleHit3D& hit2, SimpleHit3D& hit3, float kappa, float hel)
{
  float ux_in=0.;
  float uy_in=0.;
  float ux_out=0.;
  float uy_out=0.;

  float temp1=0.;
  float temp2=0.;

  xyTangent(hit1, hit2, kappa, hel, ux_in, uy_in, temp1, temp2);
  xyTangent(hit2, hit3, kappa, hel, temp1, temp2, ux_out, uy_out);

  return ux_in*ux_out + uy_in*uy_out;
}


static float dzdsSimple(SimpleHit3D& hit1, SimpleHit3D& hit2, float k)
{
  float x = hit2.x - hit1.x;
  float y = hit2.y - hit1.y;
  float D = sqrt(x*x + y*y);
  float s = 0.;
  float temp1 = k*D*0.5;temp1*=temp1;
  float temp2 = D*0.5;
  s += 2.*temp2;
  temp2*=temp1;
  s += temp2/3.;
  temp2*=temp1;
  s += (3./20.)*temp2;
  temp2*=temp1;
  s += (5./56.)*temp2;

  return (hit2.z - hit1.z)/s;
}


double Tracker::dcaToVertexXY(SimpleTrack3D& track, double vx, double vy)
{
  double d_out = 0.;

  // find point at the dca to 0
  double x0 = track.d*cos(track.phi);
  double y0 = track.d*sin(track.phi);

  // change variables so x0,y0 -> 0,0
  double phi2 = atan2((1. + track.kappa*track.d)*sin(track.phi) - track.kappa*y0, (1. + track.kappa*track.d)*cos(track.phi) - track.kappa*x0);

  // translate so that (0,0) -> (x0 - vx , y0 - vy)
  double cosphi = cos(phi2);
  double sinphi = sin(phi2);
  double tx = cosphi + track.kappa*(x0-vx);
  double ty = sinphi + track.kappa*(y0-vy);
  double dk = sqrt( tx*tx + ty*ty ) - 1.;
  if(track.kappa == 0.){d_out = (x0-vx)*cosphi + (y0-vy)*sinphi;}
  else{d_out = dk/track.kappa;}
  return fabs(d_out);
}


void Tracker::finalize(vector<SimpleTrack3D>& input, vector<SimpleTrack3D>& output)
{
  unsigned int nt = input.size();
  vector<bool> usetrack;
  usetrack.assign(input.size(), true);
  vector<double> next_best_chi2;
  next_best_chi2.assign(input.size(), 99999.);

  if(reject_ghosts == true)
  {
    // pair-based ghost rejection
    vector<hitPair> pairs;
    for(unsigned int i=0;i<nt;++i)
    {
      for(unsigned int h1=0;h1<input[i].hits.size();++h1)
      {
        for(unsigned int h2=(h1+1);h2<input[i].hits.size();++h2)
        {
          pairs.push_back(hitPair(input[i].hits[h1].index,input[i].hits[h2].index,i,track_states[i].chi2));
        }
      }
    }
    if(pairs.size() == 0){return;}
    sort(pairs.begin(), pairs.end());
    unsigned int pos=0;
    unsigned int cur_h1 = pairs[pos].hit1;
    unsigned int cur_h2 = pairs[pos].hit2;
    while(pos < pairs.size())
    {
      unsigned int next_pos = pos+1;
      if(next_pos >= pairs.size()){break;}
      while( (cur_h1 == pairs[next_pos].hit1) && (cur_h2 == pairs[next_pos].hit2) )
      {
        next_pos+=1;
        if(next_pos >= pairs.size()){break;}
      }
      if((next_pos - pos) > 1)
      {
        float best_chi2 = pairs[pos].chi2;
        float next_chi2 = pairs[pos+1].chi2;
        unsigned int best_pos = pos;
        for(unsigned int i=(pos+1);i<next_pos;++i)
        {
          if(pairs[i].chi2 < best_chi2)
          {
            next_chi2 = best_chi2;
            best_chi2 = pairs[i].chi2;
            best_pos = i;
          }
          else if(pairs[i].chi2 < next_chi2)
          {
            next_chi2 = pairs[i].chi2;
          }
        }
        for(unsigned int i=pos;i<next_pos;++i)
        {
          if(i != best_pos)
          {
            usetrack[pairs[i].track] = false;
          }
          else
          {
            next_best_chi2[pairs[i].track] = next_chi2;
          }
        }
      }
      pos = next_pos;
      cur_h1 = pairs[pos].hit1;
      cur_h2 = pairs[pos].hit2;
    }
  }

  vector<HelixKalmanState> states_new;

  for(unsigned int i=0;i<nt;++i)
  {
    if(usetrack[i] == true)
    {
      output.push_back(input[i]);
      output.back().index = (output.size() - 1);
      states_new.push_back(track_states[i]);
      isolation_variable.push_back(next_best_chi2[i]);
    }
  }
  track_states = states_new;

  if(smooth_back == true)
  {
    for(unsigned int i=0;i<output.size();++i)
    {
      for(int h=(output[i].hits.size() - 2);h>=0;--h)
      {
        kalman->addHit(output[i].hits[h], track_states[i]);
      }
      output[i].phi = track_states[i].phi;
      output[i].d = track_states[i].d;
      output[i].kappa = track_states[i].kappa;
      output[i].z0 = track_states[i].z0;
      output[i].dzdl = track_states[i].dzdl;
    }
  }

  if(verbosity > 0)
  {
    cout<<"# combinations = "<<combos.size()<<endl;
    cout<<"# fail combinations = "<<fail_combos.size()<<endl;
    cout<<"# pass combinations = "<<pass_combos.size()<<endl;
    cout<<"# fits = "<<nfits<<endl;
    cout<<"findTracks called "<<findtracksiter<<" times"<<endl;
  }
}


static bool next_combination(vector<unsigned int>& combination, const vector<unsigned int> hits_per_layer)
{
  unsigned int l = (combination.size()-1);
  while(true)
  {
    if(combination[l] < (hits_per_layer[l]-1))
    {
      combination[l]+=1;
      return true;
    }
    else
    {
      combination[l]=0;
    }
    if(l==0){return false;}
    l-=1;
  }

  return false;
}


void Tracker::find3(vector<vector<SimpleHit3D> >& layer_sorted, vector<vector<SimpleHit3D> >& tracklets, vector<HelixKalmanState>& states, const HelixRange& range)
{
  float kappa_mid = sqrt(0.5*(range.max_k + range.min_k));

  vector<SimpleHit3D> one_tracklet;
  one_tracklet.assign(3,SimpleHit3D());
  vector<unsigned int> combinations;
  combinations.assign(3, 0);
  vector<unsigned int> hits_per_layer;
  for(unsigned int l=0;l<3;++l)
  {
    unsigned int lsize = layer_sorted[l].size();
    if(lsize == 0){return;}
    hits_per_layer.push_back(lsize);
  }
  SimpleTrack3D temp_track;
  temp_track.hits.resize(3, SimpleHit3D());
  vector<unsigned int> tempcomb;
  tempcomb.assign(3,0);
  bool keep_going = true;
  while(keep_going == true)
  {
    /* create a new combination of hits */
    for(unsigned int i=0;i<3;++i)
    {
      tempcomb[i] = layer_sorted[i][combinations[i]].index;
    }
    sort(tempcomb.begin(), tempcomb.end());

    /* skip combination if it is already in the list for failed combos */
    set<vector<unsigned int> >::iterator it = fail_combos.find(tempcomb);
    if(it != fail_combos.end())
    {
      keep_going = next_combination(combinations, hits_per_layer);
      continue;
    }
    /* skip combination if is is already in the list for passed combos */
    map<vector<unsigned int>, HelixKalmanState>::iterator it2 = pass_combos.find(tempcomb);
    if(it2 != pass_combos.end())
    {
      for(unsigned int i=0;i<3;++i)
      {
        one_tracklet[i] = layer_sorted[i][combinations[i]];
      }
      tracklets.push_back(one_tracklet);
      states.push_back(it2->second);
      keep_going = next_combination(combinations, hits_per_layer);
      continue;
    }

    for(unsigned int l=0;l<3;++l)
    {
      temp_track.hits[l] = layer_sorted[l][combinations[l]];
    }

    float ang1 = cosScatter(temp_track.hits[0], temp_track.hits[1], temp_track.hits[2], kappa_mid, 1.);
    float ang2 = cosScatter(temp_track.hits[0], temp_track.hits[1], temp_track.hits[2], kappa_mid, -1.);
    float ang = 0.;
    (ang1 > ang2) ? ang = ang1 : ang = ang2;
    if(ang < 0.999)
    {
      keep_going = next_combination(combinations, hits_per_layer);
      continue;
    }

    float dzds1 = dzdsSimple(temp_track.hits[0], temp_track.hits[1], kappa_mid);
    float dzds2 = dzdsSimple(temp_track.hits[1], temp_track.hits[2], kappa_mid);
    if(fabs(dzds2 - dzds1) > 0.05)
    {
      keep_going = next_combination(combinations, hits_per_layer);
      continue;
    }
    /* try to fit the track candidate and check the chi2 of the fit to decide if the candidate
     * is an actual track segment */
    HelixKalmanState state;
    double chi2 = 10. * chi2_cut; // use a start value larger than the upper chi2 limit

    if ( use_kalman )
    {
      kalFitAll(temp_track, state);
      chi2 = state.chi2/(2.*temp_track.hits.size() - 5);
    }
    else
    {
      chi2 = fitTrack( temp_track );
    }
    /* check if chi2 of fit track is below limit */
    if(!(fabs(chi2) < chi2_cut))
    {
      fail_combos.insert(tempcomb);
      keep_going = next_combination(combinations, hits_per_layer);
      continue;
    }
    pass_combos.insert(pair<vector<unsigned int>, HelixKalmanState>(tempcomb,state));
    for(unsigned int i=0;i<3;++i)
    {
      one_tracklet[i] = layer_sorted[i][combinations[i]];
    }
    tracklets.push_back(one_tracklet);
    states.push_back(state);
    keep_going = next_combination(combinations, hits_per_layer);
  }
}


void Tracker::kalFitAll(SimpleTrack3D& track, HelixKalmanState& state)
{
  fitTrack(track);
  state.C = MatrixXf::Zero(5,5);
  state.C(0,0) = 0.1;
  state.C(1,1) = 0.1;
  state.C(2,2) = 0.01;
  state.C(3,3) = 0.1;
  state.C(4,4) = 0.1;
  state.phi = track.phi;
  if(state.phi < 0.){state.phi += 2.*M_PI;}
  state.d = track.d;
  state.kappa = track.kappa;
  state.z0 = track.z0;
  state.dzdl = track.dzdl;
  for(unsigned int h=0;h<track.hits.size();++h)
  {
    kalman->addHit(track.hits[h], state);nfits+=1;
  }
}


void Tracker::findTracks(vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
  findtracksiter += 1;
//   findTracksCombinatorial(hits,tracks,range);
  findTracksBySegments(hits,tracks,range);
}


void Tracker::findTracksCombinatorial(vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
  vector<vector<SimpleHit3D> > layer_sorted;
  vector<SimpleHit3D> one_layer;
  layer_sorted.assign(n_layers, one_layer);
  for(unsigned int i=0;i<hits.size();++i)
  {
    layer_sorted[hits[i].layer].push_back(hits[i]);
  }
  for(unsigned int l=0;l<n_layers;++l)
  {
    if(layer_sorted[l].size()==0){return;}
  }

  vector<vector<SimpleHit3D> > tracklets;
  vector<HelixKalmanState> states;
  find3(layer_sorted, tracklets, states, range);

  for(unsigned int l=3;l<n_layers;++l)
  {
    addHit(layer_sorted[l], tracklets, states, range);
  }

  vector<unsigned int> tempcomb;
  tempcomb.assign(n_layers,0);
  for(unsigned int i=0;i<tracklets.size();++i)
  {
    for(unsigned int l=0;l<n_layers;++l)
    {
      tempcomb[l] = tracklets[i][l].index;
    }
    sort(tempcomb.begin(),tempcomb.end());
    set<vector<unsigned int> >::iterator it = combos.find(tempcomb);
    if(it != combos.end()){continue;}
    combos.insert(tempcomb);
    SimpleTrack3D temp_track;
    for(unsigned int l=0;l<tracklets[i].size();++l)
    {
      temp_track.hits.push_back(tracklets[i][l]);
    }

    if ( use_kalman )
    {
      /* fill track helix parameters with Kalman results */
      temp_track.phi = states[i].phi;
      if(temp_track.phi < 0.){temp_track.phi += 2.*M_PI;}
      if(temp_track.phi > 2.*M_PI){temp_track.phi -= 2.*M_PI;}
      temp_track.d = states[i].d;
      temp_track.kappa = states[i].kappa;
      temp_track.z0 = states[i].z0;
      temp_track.dzdl = states[i].dzdl;
    }
    else
    {
      /* re-fit all hits assigned to tracks to get final track parameters without using Kalman */
      fitTrack(temp_track);
    }

    tracks.push_back(temp_track);
    track_states.push_back(states[i]);
  }
}


void Tracker::setSeedStates(vector<HelixKalmanState>& states)
{
  seed_states = states;
}


void Tracker::findSeededTracksCombinatorial(vector<SimpleTrack3D>& seeds, vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
  vector<vector<SimpleHit3D> > tracklets;
  vector<HelixKalmanState> states;
  for(unsigned int i=0;i<seeds.size();++i)
  {
    tracklets.push_back(seeds[i].hits);
    states.push_back(seed_states[seeds[i].index]);
  }
  vector<vector<SimpleHit3D> > layer_sorted;
  vector<SimpleHit3D> one_layer;
  layer_sorted.assign(2, one_layer);
  for(unsigned int i=0;i<hits.size();++i)
  {
    layer_sorted[hits[i].layer - 4].push_back(hits[i]);
  }
  for(unsigned int l=0;l<2;++l)
  {
    if(layer_sorted[l].size()==0){return;}
  }
  addHit(layer_sorted[0], tracklets, states, range);
  addHit(layer_sorted[1], tracklets, states, range);

  vector<unsigned int> tempcomb;
  tempcomb.assign(n_layers,0);
  for(unsigned int i=0;i<tracklets.size();++i)
  {
    for(unsigned int l=0;l<n_layers;++l)
    {
      tempcomb[l] = tracklets[i][l].index;
    }
    sort(tempcomb.begin(),tempcomb.end());
    set<vector<unsigned int> >::iterator it = combos.find(tempcomb);
    if(it != combos.end()){continue;}
    combos.insert(tempcomb);
    SimpleTrack3D temp_track;
    for(unsigned int l=0;l<tracklets[i].size();++l)
    {
      temp_track.hits.push_back(tracklets[i][l]);
    }
    temp_track.phi = states[i].phi;
    temp_track.d = states[i].d;
    temp_track.kappa = states[i].kappa;
    temp_track.z0 = states[i].z0;
    temp_track.dzdl = states[i].dzdl;
    tracks.push_back(temp_track);
    track_states.push_back(states[i]);
  }
}


void Tracker::findSeededTracks(vector<SimpleTrack3D>& seeds, vector<SimpleHit3D>& hits, vector<SimpleTrack3D>& tracks, const HelixRange& range)
{
  findSeededTracksbySegments(seeds, hits, tracks, range);
}


void Tracker::addHit(vector<SimpleHit3D>& hits, vector<vector<SimpleHit3D> >& tracklets, vector<HelixKalmanState>& states, const HelixRange& range)
{
  vector<vector<SimpleHit3D> > tracklets_new;
  vector<HelixKalmanState> states_new;

  SimpleTrack3D temp_track; // needed here only if Kalman is not used

  for(unsigned int i=0;i<tracklets.size();++i)
  {
    vector<unsigned int> tempcomb;

    for(unsigned int j=0;j<tracklets[i].size();++j)
    {
      tempcomb.push_back(tracklets[i][j].index);

      if ( ! use_kalman )
        temp_track.hits.push_back(tracklets[i][j]);
    }

    for(unsigned int h=0;h<hits.size();++h)
    {
      vector<unsigned int> tempcomb2 = tempcomb;
      tempcomb2.push_back(hits[h].index);
      sort(tempcomb2.begin(), tempcomb2.end());

      /* skip hit combination if is is already on the failed combinations list */
      set<vector<unsigned int> >::iterator it = fail_combos.find(tempcomb2);
      if(it != fail_combos.end()){continue;}

      /* skip hit combination if is is already on the passed combinations list */
      map<vector<unsigned int>, HelixKalmanState>::iterator it2 = pass_combos.find(tempcomb2);
      if(it2 != pass_combos.end())
      {
        vector<SimpleHit3D> one_tracklet = tracklets[i];
        one_tracklet.push_back(hits[h]);
        tracklets_new.push_back(one_tracklet);
        states_new.push_back(it2->second);
        continue;
      }

      float ang1 = cosScatter(tracklets[i][tracklets[i].size()-2], tracklets[i][tracklets[i].size()-1], hits[h], states[i].kappa, 1.);
      float ang2 = cosScatter(tracklets[i][tracklets[i].size()-2], tracklets[i][tracklets[i].size()-1], hits[h], states[i].kappa, -1.);
      float ang = 0.;
      (ang1 > ang2) ? ang = ang1 : ang = ang2;
      if(ang < 0.9)
      {
        continue;
      }

      /* try to fit the track candidate after adding another hit and check the chi2 of the fit to decide if the added hit
       * actually is part of the track segment */
      HelixKalmanState state = states[i];
      double chi2 = 10. * chi2_cut;

      if ( use_kalman )
      {
        kalman->addHit(hits[h], state);nfits+=1;
        chi2 = state.chi2/(2.*(tracklets[i].size() + 1) - 5);
      }
      else
      {
        SimpleTrack3D temp_track2 = temp_track;
        temp_track2.hits.push_back(hits[h]);
        chi2 = fitTrack( temp_track2 );
      }

      /* check if chi2 of fit track is below limit */
      if(!(fabs(chi2) < chi2_cut))
      {
        fail_combos.insert(tempcomb2);
        continue;
      }
      pass_combos.insert(pair<vector<unsigned int>, HelixKalmanState>(tempcomb2,state));
      vector<SimpleHit3D> one_tracklet = tracklets[i];
      one_tracklet.push_back(hits[h]);
      tracklets_new.push_back(one_tracklet);
      states_new.push_back(state);
    }
  }

  tracklets = tracklets_new;
  states = states_new;
}


bool Tracker::breakRecursion(const vector<SimpleHit3D>& hits, const HelixRange& range)
{
  if(seeding == true){return false;}
  unsigned int layer_mask = 0;
  for(unsigned int i=0;i<hits.size();++i)
  {
    layer_mask = layer_mask | (1<<hits[i].layer);
  }
  unsigned int nlayers = __builtin_popcount(layer_mask);
  return (nlayers < required_layers);

}


float Tracker::phiError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl)
{
  float Bfield_inv = 1.0f/geometry.detector_B_field;
  float p_inv=0.;
  if(seeding == false)
  {
    if((prev_max_k==max_k) && (prev_max_dzdl==max_dzdl))
    {
      p_inv=prev_p_inv;
    }
    else
    {
      prev_max_k=max_k;
      prev_max_dzdl=max_dzdl;
      prev_p_inv = 3.33333333333333314e+02*max_k*Bfield_inv*sqrt(1. - max_dzdl*max_dzdl);
      p_inv=prev_p_inv;
    }
    float total_scatter_2 = 0.;
    for(unsigned int i=1;i<=(unsigned)(hit.layer);++i)
    {
      float this_scatter = geometry.detector_scatter[i-1]*(geometry.detector_radii[i]-geometry.detector_radii[i-1])/geometry.detector_radii[i];
      total_scatter_2 += this_scatter*this_scatter;
    }
    float angle = p_inv*sqrt(total_scatter_2)*1.0;
    float dsize = 0.5*(max_d-min_d);
    float angle_from_d = dsize/geometry.detector_radii[hit.layer];
    float returnval = 0.;
    if(angle_from_d > angle){returnval=0.;}
    else{returnval = (angle - angle_from_d);}

    return returnval;
  }
  else
  {
    p_inv = 3.33333333333333314e+02*max_k*Bfield_inv*sqrt(1. - max_dzdl*max_dzdl);
    float scatter = p_inv*geometry.detector_scatter[3]*1.0;
    return scatter + 0.002;
  }
}


float Tracker::dzdlError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl)
{
  float Bfield_inv = 1.0f/geometry.detector_B_field;
  float p_inv=0.;
  if(seeding == false)
  {
    if((prev_max_k==max_k) && (prev_max_dzdl==max_dzdl))
    {
      p_inv=prev_p_inv;
    }
    else
    {
      prev_max_k=max_k;
      prev_max_dzdl=max_dzdl;
      prev_p_inv = 3.33333333333333314e+02*max_k*Bfield_inv*sqrt(1. - max_dzdl*max_dzdl);
      p_inv=prev_p_inv;
    }
    float total_scatter_2 = 0.;
    for(unsigned int i=1;i<=(unsigned)(hit.layer);++i)
    {
      float this_scatter = geometry.detector_scatter[i-1]*(geometry.detector_radii[i]-geometry.detector_radii[i-1])/geometry.detector_radii[i];
      total_scatter_2 += this_scatter*this_scatter;
    }
    float angle = p_inv*sqrt(total_scatter_2)*1.0;
    float z0size = 0.5*(max_z0-min_z0);
    float angle_from_z0 = z0size/geometry.detector_radii[hit.layer];
    float returnval = 0.;
    if(angle_from_z0 > angle){returnval=0.;}
    else{returnval = (angle - angle_from_z0);}

    return returnval;
  }
  else
  {
    p_inv = 3.33333333333333314e+02*max_k*Bfield_inv*sqrt(1. - max_dzdl*max_dzdl);
    float scatter = p_inv*geometry.detector_scatter[3]*1.0;
    return scatter + 0.002;
  }
}





