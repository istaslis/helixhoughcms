#ifndef __SPHENIXTRACKER__
#define __SPHENIXTRACKER__

#include "HelixHough.h"
#include <vector>
#include <set>
#include <map>
#include <string>
#include "CylinderKalman.h"
#include "HelixKalmanState.h"
#include "DetectorGeometry.h"


class Tracker : public HelixHough
{
public:
  Tracker(std::string detectorfilename, unsigned int n_phi, unsigned int n_d, unsigned int n_k, unsigned int n_dzdl, unsigned int n_z0, HelixResolution& min_resolution, HelixResolution& max_resolution, HelixRange& range);
  Tracker(std::string detectorfilename, std::vector<std::vector<unsigned int> >& zoom_profile, unsigned int minzoom, HelixRange& range);
  ~Tracker();

  void finalize(std::vector<SimpleTrack3D>& input, std::vector<SimpleTrack3D>& output);
  void findTracks(std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);
  void findTracksBySegments(std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);
  void findTracksCombinatorial(std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);
  void initEvent(std::vector<SimpleHit3D>& hits, unsigned int min_hits)
  {
    nfits=0;
    combos.clear();
    fail_combos.clear();
    pass_combos.clear();
    track_states.clear();
    seeding=false;
    required_layers = min_hits;
    isolation_variable.clear();
    findtracksiter = 0;
  }
  void initSeeding()
  {
    seeding=true;
  }

  /** Find 3-hit track segment candidates (3 hits is the minimum to fit a helix to the hits) by trying various combinations of hits (track finding).
   * The input to this function is a subset of hits identified by the Hough algorithm as hits belonging to the same track.
   */
  void find3(std::vector<std::vector<SimpleHit3D> >& layer_sorted, std::vector<std::vector<SimpleHit3D> >& tracklets, std::vector<HelixKalmanState>& states, const HelixRange& range);

  /** Try to extend previously found 3-hit track segements (mini tracks, tracklets) by adding hits from other layers. Check chi2 change for each added hit
   * to judge if the hit actually belongs to the same track (keep hit) or not (reject hit).
   */
  void addHit(std::vector<SimpleHit3D>& hits, std::vector<std::vector<SimpleHit3D> >& tracklets, std::vector<HelixKalmanState>& states, const HelixRange& range);

  void findSeededTracks(std::vector<SimpleTrack3D>& seeds, std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);
  void findSeededTracksCombinatorial(std::vector<SimpleTrack3D>& seeds, std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);
  void findSeededTracksbySegments(std::vector<SimpleTrack3D>& seeds, std::vector<SimpleHit3D>& hits, std::vector<SimpleTrack3D>& tracks, const HelixRange& range);

  /** Use Kalman to fit helix to a track candidate (list of hits). The input is a track object (with arbitrary track parameters) which includes the hits of
   * the track candidate. The function fills the track parameters based on the fit result.
   */
  void kalFitAll(SimpleTrack3D& track, HelixKalmanState& state);

  double dcaToVertexXY(SimpleTrack3D& track, double vx, double vy);

  /** Define a chi2 cut that track fits have to pass to be accepted
   */
  void setChi2Cut(double c){chi2_cut=c;}

  bool breakRecursion(const std::vector<SimpleHit3D>& hits, const HelixRange& range);

  float phiError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl);
  float dzdlError(SimpleHit3D& hit, float min_k, float max_k, float min_d, float max_d, float min_z0, float max_z0, float min_dzdl, float max_dzdl);

  /** Fit a helix to a track candidate (list of hits). The input is a track object (with arbitrary track parameters) which includes the hits of the track candidate.
   * The function fills the track parameters based on the fit result and returns the chi2 of the fit.
   */
  static float fitTrack(SimpleTrack3D& track);
  static float fitTrack_3(SimpleTrack3D& track);

  void setNLayers(unsigned int n){n_layers=n;}
  void setNSeedLayers(unsigned int n){n_SeedLayers=n;}

  void setVerbosity(int v){verbosity=v;}

  void setCutOnDca(bool dcut){cut_on_dca = dcut;}
  void setDcaCut(double dcut){dca_cut = dcut;}
  void setVertex(double vx,double vy,double vz){vertex_x=vx;vertex_y=vy;vertex_z=vz;}

  void setRejectGhosts(bool rg){reject_ghosts = rg;}

  void setSeedStates(std::vector<HelixKalmanState>& states);

  std::vector<HelixKalmanState>& getKalmanStates(){return track_states;}
  std::vector<double>& getIsolation(){return isolation_variable;}

  void setSmoothBack(bool sb){smooth_back=sb;}

  /** Switch for Kalman:
   * TRUE = Use Kalman for fitting (requires correct implementation of detector geometry, better results)
   * FALSE = simple fitting (independent of detector geometry).
   */
  void setUseKalman( bool flag ){ use_kalman = flag; }

  /** Define detector geometry needed for Kalman filter and create a new Kalman object. Current implementations:
   * - sPHENIX: sPHENIX detector design (Default geometry)
   * - PHENIXsimple: Approximation of PHENIX detector assuming cylindric vertex detector layers with full angular coverage
   */
  void defineDetector( std::string det_name );
  
  static void calculateKappaTangents(float* x1_a, float* y1_a, float* z1_a, float* x2_a, float* y2_a, float* z2_a, float* x3_a, float* y3_a, float* z3_a, float* dx1_a, float* dy1_a, float* dz1_a, float* dx2_a, float* dy2_a, float* dz2_a, float* dx3_a, float* dy3_a, float* dz3_a, float* kappa_a, float* dkappa_a, float* ux_mid_a, float* uy_mid_a, float* ux_end_a, float* uy_end_a, float* dzdl_1_a, float* dzdl_2_a, float* ddzdl_1_a, float* ddzdl_2_a);


private:

  unsigned int n_layers;
  unsigned int n_SeedLayers;
  
  double chi2_cut;
  std::set<std::vector<unsigned int> > combos;
  std::map<std::vector<unsigned int>, HelixKalmanState > pass_combos;
  std::set<std::vector<unsigned int> > fail_combos;


  bool seeding;
  std::vector<HelixKalmanState> track_states;
  int verbosity;
  bool cut_on_dca;
  double dca_cut;
  double vertex_x, vertex_y, vertex_z;
  unsigned int required_layers;
  bool reject_ghosts;
  unsigned int nfits;
  bool smooth_back;

  DetectorGeometry geometry;

  std::vector<HelixKalmanState> seed_states;
  CylinderKalman* kalman;

  std::vector<double> isolation_variable;

  /** Switch for Kalman:
   * TRUE = Use Kalman for fitting (requires correct implementation of detector geometry, better results)
   * FALSE = simple fitting (independent of detector geometry).
   */
  bool use_kalman;
  
  unsigned int findtracksiter;
  
  float prev_max_k;
  float prev_max_dzdl;
  float prev_p_inv;
};


#endif
