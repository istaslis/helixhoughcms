/*
 * DetectorGeometry.cpp
 *
 *  Created on: Apr 23, 2013
 *      Author: istaslis
 */
#include "DetectorGeometry.h"

using namespace std;

const double sqrt_12_inv = 1.0/sqrt(12.);

void DetectorGeometry::ReadFromFile(string filename)
{
	double radius = 0.;
	double radlen = 0.;
	double xr = 0.;
	double zr = 0.;
	unsigned int layersNumber = 0;

	detector_radii.clear();
	detector_scatter.clear();
	integrated_scatter.clear();
	detector_material.clear();

	ifstream det_in;
	det_in.open(filename.c_str());

	cout << filename.c_str()<<endl;

	det_in >> detector_B_field >> radius >> radlen >> xr >> zr;
	while (det_in.good())
	{
		layersNumber++;
		detector_radii.push_back(radius);
		detector_scatter.push_back(1.41421356237309515 * 0.0136 * sqrt(radlen));
		detector_material.push_back(radlen);

		smear_xy_layer.push_back(xr * sqrt_12_inv);
		smear_z_layer.push_back(zr * sqrt_12_inv);

		det_in >> radius >> radlen >> xr >> zr;

		cout <<  radius << radlen << xr << zr;
	}
	integrated_scatter.assign(layersNumber, 0.);
	float total_scatter_2 = 0.;
	for (int l = 0; l < layersNumber; ++l)
	{
		total_scatter_2 += detector_scatter[l] * detector_scatter[l];
		integrated_scatter[l] = sqrt(total_scatter_2);
	}


	LayersNumber = layersNumber;

}

DetectorGeometry::DetectorGeometry(string filename)
{
	ReadFromFile(filename);

}
