/*
 * DetectorGeometry.h
 *
 *  Created on: Apr 23, 2013
 *      Author: istaslis
 */

#ifndef DETECTORGEOMETRY_H_
#define DETECTORGEOMETRY_H_

#include <string>
#include <vector>
#include <math.h>
#include <fstream>
#include <iostream>

class DetectorGeometry
{
public:
	double detector_B_field;
	std::vector<float> detector_material; //not sure really what is
	std::vector<float> detector_scatter;  // the difference between the two
	std::vector<float> integrated_scatter;
	std::vector<float> detector_radii;

	std::vector<float> smear_xy_layer;
	std::vector<float> smear_z_layer;

	unsigned int LayersNumber;

	void ReadFromFile(std::string filename);

	DetectorGeometry(std::string filename);
	DetectorGeometry():detector_B_field(0.),LayersNumber(0){};
};

#endif /* DETECTORGEOMETRY_H_ */
